# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Báo cáo lỗi
clear-all-reports-label = Xóa toàn bộ báo cáo
delete-confirm-title = Bạn có chắc không?
delete-confirm-description = Điều này sẽ xóa tất cả báo cáo và không thể hoàn tác.
crashes-unsubmitted-label = Các báo cáo lỗi chưa gửi
id-heading = Định danh báo cáo
date-crashed-heading = Ngày xảy ra lỗi
crashes-submitted-label = Các báo cáo lỗi đã gửi
date-submitted-heading = Ngày gửi
no-reports-label = Chưa có báo cáo lỗi nào được gửi.
no-config-label = Ứng dụng này không thể hiển thị các báo cáo lỗi. Tùy chỉnh <code>breakpad.reportURL</code> phải được thiết lập.
