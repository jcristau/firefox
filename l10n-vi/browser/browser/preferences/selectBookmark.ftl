# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

select-bookmark-window =
    .title = Đặt làm Trang Chủ
    .style = width: 32em;
select-bookmark-desc = Chọn một Trang đánh dấu để làm Trang Chủ của bạn. Nếu bạn chọn một thư mục, các Trang đánh dấu của thư mục đó sẽ được mở trong Thẻ.
