# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Správy o zlyhaní
clear-all-reports-label = Odstrániť všetky správy
delete-confirm-title = Naozaj chcete odstrániť?
delete-confirm-description = Týmto odstránite všetky správy o zlyhaní. Akciu nie je možné vrátiť späť.
crashes-unsubmitted-label = Neodoslané správy o zlyhaní
id-heading = Identifikátor správy
date-crashed-heading = Dátum zlyhania
crashes-submitted-label = Odoslané správy o zlyhaní
date-submitted-heading = Dátum odoslania
no-reports-label = Neboli odoslané žiadne správy o zlyhaní.
no-config-label = Táto aplikácia nebola nastavená na zobrazovanie správ o zlyhaní. Musí byť nastavená predvoľba <code>breakpad.reportURL</code>.
