# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Lingvoj
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Retpaĝoj estas kelkfoje proponitaj en pli ol unu lingvo. Elektu la lingvojn por montri tiujn paĝojn, ordigitaj laŭ prefero.
languages-customize-spoof-english =
    .label = Peti paĝojn en la angla por plibonigi privatecon
languages-customize-moveup =
    .label = Movi supren
    .accesskey = S
languages-customize-movedown =
    .label = Movi malsupren
    .accesskey = M
languages-customize-remove =
    .label = Forigi
    .accesskey = F
languages-customize-select-language =
    .placeholder = Elektu lingvon aldonotan…
languages-customize-add =
    .label = Aldoni
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
