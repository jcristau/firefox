# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Raportoj pri paneo
clear-all-reports-label = Forigi ĉiujn raportojn
delete-confirm-title = Ĉu vi certas?
delete-confirm-description = Tiu nemalfarebla ago forviŝos ĉiujn raportojn.
crashes-unsubmitted-label = Nesenditaj raportoj pri paneo
id-heading = ID de raporto
date-crashed-heading = Dato de paneo
crashes-submitted-label = Senditaj raportoj pri paneo
date-submitted-heading = Dato de sendo
no-reports-label = Neniu raporto estis sendita.
no-config-label = Tiu ĉi programo ne estis agordita por montri raportojn de paneo. La prefero <code>breakpad.reportURL</code> devas esti aktiva.
