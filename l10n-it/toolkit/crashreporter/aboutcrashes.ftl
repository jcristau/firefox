# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Segnalazioni di arresto anomalo
clear-all-reports-label = Rimuovi tutte le segnalazioni
delete-confirm-title = Eliminare le segnalazioni?
delete-confirm-description = Verranno eliminate tutte le segnalazioni e questa operazione non può essere annullata.
crashes-unsubmitted-label = Segnalazioni non ancora inviate
id-heading = ID segnalazione
date-crashed-heading = Data crash
crashes-submitted-label = Segnalazioni inviate
date-submitted-heading = Data invio
no-reports-label = Non è stata inviata alcuna segnalazione.
no-config-label = Questa applicazione non è stata configurata per visualizzare le segnalazioni inviate. È necessario impostare il parametro <code>breakpad.reportURL</code>.
