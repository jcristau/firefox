# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Stratégies d’entreprise
errors-tab = Erreurs
documentation-tab = Documentation
policy-name = Nom de la stratégie
policy-value = Contenu de la stratégie
policy-errors = Erreurs de stratégie
