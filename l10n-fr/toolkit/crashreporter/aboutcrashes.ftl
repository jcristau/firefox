# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Rapports de plantage
clear-all-reports-label = Supprimer tous les rapports
delete-confirm-title = Confirmation
delete-confirm-description = Ceci supprimera tous les rapports irrémédiablement.
crashes-unsubmitted-label = Rapports de plantage non envoyés
id-heading = Identifiant du rapport
date-crashed-heading = Date du plantage
crashes-submitted-label = Rapports de plantage envoyés
date-submitted-heading = Date d’envoi
no-reports-label = Aucun rapport de plantage n’a été envoyé.
no-config-label = Cette application n’est pas configurée pour afficher les rapports de plantage. Vous devez définir la variable <code>breakpad.reportURL</code>.
