# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = 企业策略
# 'Active' is used to describe the policies that are currently active
active-policies-tab = 活跃
errors-tab = 错误
documentation-tab = 文档
policy-name = 策略名
policy-value = 策略值
policy-errors = 策略错误
