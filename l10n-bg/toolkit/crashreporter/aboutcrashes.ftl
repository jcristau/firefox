# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Доклади за сривове
clear-all-reports-label = Премахване на всички
delete-confirm-title = Сигурни ли сте?
delete-confirm-description = Така ще изтриете всички доклади, а това е необратимо.
crashes-unsubmitted-label = Неизпратени доклади за срив
id-heading = Идентификатор на доклад
date-crashed-heading = Дата на срива
crashes-submitted-label = Изпратени доклади за срив
date-submitted-heading = Дата на изпращане
no-reports-label = Няма изпратени доклади за срив.
no-config-label = Приложението не е настроено да показва доклади за сривове. За това настройката <code>breakpad.reportURL</code> трябва да бъде зададена.
