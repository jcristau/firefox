# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Reportos de collapso
clear-all-reports-label = Remover tote le reportos
delete-confirm-title = Es tu secur?
delete-confirm-description = Isto delera tote le reportos e essera irreversibile.
crashes-unsubmitted-label = Reportos de collapso non submittite
id-heading = Identificator del reporto
date-crashed-heading = Data del collapso
crashes-submitted-label = Reportos de collapso submittite
date-submitted-heading = Data del submission
no-reports-label = Nulle reportos de collapso esseva submittite.
no-config-label = Iste application non ha essite configurate pro monstrar le reportos de collapso. Le preferentia <code>breakpad.reportURL</code> debe esser activate.
