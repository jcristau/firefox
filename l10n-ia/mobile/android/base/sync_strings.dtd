<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Connecter se a &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Pro activar tu nove apparato, selige “Configurar &syncBrand.shortName.label;” in le apparato.'>
<!ENTITY sync.subtitle.pair.label 'Activar, selige “Copula un apparato” in vostre apparato altere.'>
<!ENTITY sync.pin.default.label '..\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Io non habe le apparato con me…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Authenticationes'>
<!ENTITY sync.configure.engines.title.history 'Chronologia'>
<!ENTITY sync.configure.engines.title.tabs 'Schedas'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; in &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Menu de marcapaginas'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Etiquettas'>
<!ENTITY bookmarks.folder.toolbar.label 'Barra de marcapaginas'>
<!ENTITY bookmarks.folder.other.label 'Altere marcapaginas'>
<!ENTITY bookmarks.folder.desktop.label 'Marcapaginas del scriptorio'>
<!ENTITY bookmarks.folder.mobile.label 'Marcapaginas del mobile'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Clavate'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Retornar al navigation'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Benvenite a &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Authentica te pro synchronisar tu schedas, marcapaginas, authenticationes e plus.'>
<!ENTITY fxaccount_getting_started_get_started 'Comenciar'>
<!ENTITY fxaccount_getting_started_old_firefox 'Esque usante un version vetule de &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Servator de conto'>
<!ENTITY fxaccount_status_sync_now 'Synchronizar ora'>
<!ENTITY fxaccount_status_syncing2 'Synchronizante…'>
<!ENTITY fxaccount_status_device_name 'Nomine de apparato'>
<!ENTITY fxaccount_status_sync_server 'Servator de Sync'>
<!ENTITY fxaccount_status_needs_verification2 'Vostre conto besonia ser verificate. Toccar pro reinviar le email de verification.'>
<!ENTITY fxaccount_status_needs_credentials 'Non se pote connecter. Tocca pro initiar session.'>
<!ENTITY fxaccount_status_needs_upgrade 'Vos besonia actualizar &brandShortName; pro se connecter.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; es configurate, ma non auto-synchronisante. Activa “Auto-sync datos” en parametros de Android &gt; Usage de Datos.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; es configurate, ma non auto-synchronisante. Activa “Auto-sync datos” en le menu de parametros de Android &gt; Contos.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Tocca pro authenticar te con tu nove conto Firefox.'>
<!ENTITY fxaccount_status_choose_what 'Elige que synchronisar'>
<!ENTITY fxaccount_status_bookmarks 'Marcapaginas'>
<!ENTITY fxaccount_status_history 'Chronologia'>
<!ENTITY fxaccount_status_passwords2 'Authenticationes'>
<!ENTITY fxaccount_status_tabs 'Schedas aperite'>
<!ENTITY fxaccount_status_additional_settings 'Parametros additional'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Sync solo super Wi-Fi'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Impedir a &brandShortName; de synchronisar super un cellular o super un rete pagate a uso'>
<!ENTITY fxaccount_status_legal 'Legal' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Terminos del servicio'>
<!ENTITY fxaccount_status_linkprivacy2 'Notification de confidentialitate'>
<!ENTITY fxaccount_remove_account 'Disconnecter&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Disconnecter se de Sync?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Vostre datos de navigation se remanera in este apparato, ma non se synchronizara plus con vostre conto.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Conto Firefox &formatS; disconnectite.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Disconnecter'>

<!ENTITY fxaccount_enable_debug_mode 'Habilitar Modo de Depuration'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title 'Optiones de &syncBrand.shortName.label;'>
<!ENTITY fxaccount_options_configure_title 'Configurar &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; non es connectite'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Tocca pro authenticar te como &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Esque finir le actualization de &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Toccar pro authenticar te como &formatS;'>
