# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Linguas
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Paginas web es alcun vices offerite in plus de un lingua. Selige le linguas pro presentar iste paginas web, in ordine de preferentia
languages-customize-spoof-english =
    .label = Demanda le versiones anglese del paginas web pro major confidentialitate
languages-customize-moveup =
    .label = Ascender
    .accesskey = A
languages-customize-movedown =
    .label = Descender
    .accesskey = D
languages-customize-remove =
    .label = Remover
    .accesskey = R
languages-customize-select-language =
    .placeholder = Selige un lingua a adder…
languages-customize-add =
    .label = Adder
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale } [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
