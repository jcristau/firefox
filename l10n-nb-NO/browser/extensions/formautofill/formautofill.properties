# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (saveAddressesMessage): %S is brandShortName. This string is used on the doorhanger to
# notify users that addresses are saved.
saveAddressesMessage = %S lagrer nå adresser slik at du kan fylle ut skjemaer raskere.
# LOCALIZATION NOTE (autofillOptionsLink, autofillOptionsLinkOSX): These strings are used in the doorhanger for
# updating addresses. The link leads users to Form Autofill browser preferences.
autofillOptionsLink = Innstillinger for autoutfylling av skjema
autofillOptionsLinkOSX = Innstillinger for autoutfylling av skjema
# LOCALIZATION NOTE (autofillSecurityOptionsLink, autofillSecurityOptionsLinkOSX): These strings are used
# in the doorhanger for saving credit card info. The link leads users to Form Autofill browser preferences.
autofillSecurityOptionsLink = Innstillinger for autoutfylling av skjema og sikkerhet
autofillSecurityOptionsLinkOSX = Innstillinger for autoutfylling av skjema og sikkerhet
# LOCALIZATION NOTE (changeAutofillOptions, changeAutofillOptionsOSX): These strings are used on the doorhanger
# that notifies users that addresses are saved. The button leads users to Form Autofill browser preferences.
changeAutofillOptions = Endre innstillinger for autoutfylling av skjema
changeAutofillOptionsOSX = Endre innstillinger for autoutfylling av skjema
changeAutofillOptionsAccessKey = n
# LOCALIZATION NOTE (addressesSyncCheckbox): If Sync is enabled, this checkbox is displayed on the doorhanger
# shown when saving addresses.
addressesSyncCheckbox = Del adresser med synkroniserte enheter
# LOCALIZATION NOTE (creditCardsSyncCheckbox): If Sync is enabled and credit card sync is available,
# this checkbox is displayed on the doorhanger shown when saving credit card.
creditCardsSyncCheckbox = Del kredittkort med synkroniserte enheter
# LOCALIZATION NOTE (updateAddressMessage, updateAddressDescriptionLabel, createAddressLabel, updateAddressLabel):
# Used on the doorhanger when an address change is detected.
updateAddressMessage = Vil du oppdatere din adresse med denne nye informasjonen?
updateAddressDescriptionLabel = Adresse å oppdatere:
createAddressLabel = Opprett ny adresse
createAddressAccessKey = O
updateAddressLabel = Oppdater adresse
updateAddressAccessKey = A
# LOCALIZATION NOTE (saveCreditCardMessage, saveCreditCardDescriptionLabel, saveCreditCardLabel, cancelCreditCardLabel, neverSaveCreditCardLabel):
# Used on the doorhanger when users submit payment with credit card.
# LOCALIZATION NOTE (saveCreditCardMessage): %S is brandShortName.
saveCreditCardMessage = Vil du at %S lagrer ditt kredittkort? (Sikkerhetskoden lagres ikke)
saveCreditCardDescriptionLabel = Kredittkort å lagre:
saveCreditCardLabel = Lagre kredittkort
saveCreditCardAccessKey = l
cancelCreditCardLabel = Ikke lagre
cancelCreditCardAccessKey = a
neverSaveCreditCardLabel = Lagre aldri kredittkort
neverSaveCreditCardAccessKey = a
# LOCALIZATION NOTE (updateCreditCardMessage, updateCreditCardDescriptionLabel, createCreditCardLabel, updateCreditCardLabel):
# Used on the doorhanger when an credit card change is detected.
updateCreditCardMessage = Vil du oppdatere kredittkortet ditt med denne nye informasjonen?
updateCreditCardDescriptionLabel = Kredittkort å oppdatere:
createCreditCardLabel = Lag et nytt kredittkort
createCreditCardAccessKey = L
updateCreditCardLabel = Oppdater kredittkort
updateCreditCardAccessKey = O
# LOCALIZATION NOTE (openAutofillMessagePanel): Tooltip label for Form Autofill doorhanger icon on address bar.
openAutofillMessagePanel = Åpne meldingspanel for autoutfylling av skjema

# LOCALIZATION NOTE ( (autocompleteFooterOptionShort, autocompleteFooterOptionOSXShort): Used as a label for the button,
# displayed at the bottom of the dropdown suggestion, to open Form Autofill browser preferences.
autocompleteFooterOptionShort = Flere innstillinger
autocompleteFooterOptionOSXShort = Innstillinger
# LOCALIZATION NOTE (category.address, category.name, category.organization2, category.tel, category.email):
# Used in autofill drop down suggestion to indicate what other categories Form Autofill will attempt to fill.
category.address = adresse
category.name = navn
category.organization2 = organisasjon
category.tel = telefon
category.email = e-post
# LOCALIZATION NOTE (fieldNameSeparator): This is used as a separator between categories.
fieldNameSeparator = ,\u0020
# LOCALIZATION NOTE (phishingWarningMessage, phishingWarningMessage2): The warning
# text that is displayed for informing users what categories are about to be filled.
# "%S" will be replaced with a list generated from the pre-defined categories.
# The text would be e.g. Also autofills organization, phone, email.
phishingWarningMessage = Fyller også automatisk ut %S
phishingWarningMessage2 = Fyller ut automatisk %S
# LOCALIZATION NOTE (insecureFieldWarningDescription): %S is brandShortName. This string is used in drop down
# suggestion when users try to autofill credit card on an insecure website (without https).
insecureFieldWarningDescription = %S har oppdaget et usikkert nettsted. Autoutfylling av skjemaer er midlertidig deaktivert.
# LOCALIZATION NOTE (clearFormBtnLabel2): Label for the button in the dropdown menu that used to clear the populated
# form.
clearFormBtnLabel2 = Tøm autoutfylt skjema

# LOCALIZATION NOTE (autofillAddressesCheckbox): Label for the checkbox that enables autofilling addresses.
autofillAddressesCheckbox = Autofyll adresser
# LOCALIZATION NOTE (learnMoreLabel): Label for the link that leads users to the Form Autofill SUMO page.
learnMoreLabel = Les mer
# LOCALIZATION NOTE (savedAddressesBtnLabel): Label for the button that opens a dialog that shows the
# list of saved addresses.
savedAddressesBtnLabel = Lagrede adresser…
# LOCALIZATION NOTE (autofillCreditCardsCheckbox): Label for the checkbox that enables autofilling credit cards.
autofillCreditCardsCheckbox = Autofyll kredittkort
# LOCALIZATION NOTE (savedCreditCardsBtnLabel): Label for the button that opens a dialog that shows the list
# of saved credit cards.
savedCreditCardsBtnLabel = Lagrede kredittkort…

# LOCALIZATION NOTE (manageAddressesTitle, manageCreditCardsTitle): The dialog title for the list of addresses or
# credit cards in browser preferences.
manageAddressesTitle = Lagrede adresser
manageCreditCardsTitle = Lagrede kredittkort
# LOCALIZATION NOTE (addressesListHeader, creditCardsListHeader): The header for the list of addresses or credit cards
# in browser preferences.
addressesListHeader = Adresser
creditCardsListHeader = Kredittkort
showCreditCardsBtnLabel = Vis kredittkort
hideCreditCardsBtnLabel = Skjul kredittkort
removeBtnLabel = Fjern
addBtnLabel = Legg til…
editBtnLabel = Rediger…
# LOCALIZATION NOTE (manageDialogsWidth): This strings sets the default width for windows used to manage addresses and
# credit cards.
manageDialogsWidth = 560px

# LOCALIZATION NOTE (addNewAddressTitle, editAddressTitle): The dialog title for creating or editing addresses
# in browser preferences.
addNewAddressTitle = Legg til ny adresse
editAddressTitle = Rediger adresse
givenName = Fornavn
additionalName = Mellomnavn
familyName = Etternavn
organization2 = Organisasjon
streetAddress = Gateadresse
city = Sted
province = Fylke
state = Stat
postalCode = Postnummer
zip = Postnummer
country = Land eller region
tel = Telefon
email = E-post
cancelBtnLabel = Avbryt
saveBtnLabel = Lagre
countryWarningMessage2 = Automatisk utfylling av skjema er for tiden bare tilgjengelig i enkelte land.

# LOCALIZATION NOTE (addNewCreditCardTitle, editCreditCardTitle): The dialog title for creating or editing
# credit cards in browser preferences.
addNewCreditCardTitle = Legg til nytt kredittkort
editCreditCardTitle = Rediger kredittkort
cardNumber = Kortnummer
invalidCardNumber = Skriv inn et gyldig kortnummer
nameOnCard = Navn på kort
cardExpires = Utløpsdato
cardExpiresMonth = Utløpsmåned
cardExpiresYear = Utløpsår
billingAddress = Fakturaadresse
