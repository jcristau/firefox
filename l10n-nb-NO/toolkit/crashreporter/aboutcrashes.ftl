# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Krasjrapporter
clear-all-reports-label = Slett alle rapporter
delete-confirm-title = Er du sikker?
delete-confirm-description = Dette vil slette alle rapporter, og kan ikke omgjøres.
crashes-unsubmitted-label = Ikke-innsendte krasjrapporter
id-heading = Rapport-ID
date-crashed-heading = Dato for krasj
crashes-submitted-label = Sendte krasjrapporter
date-submitted-heading = Dato sendt
no-reports-label = Ingen krasjrapporter er sendt.
no-config-label = Dette programmet er ikke konfigurert for å vise krasjrapporter. Innstillingen <code>breakpad.reportURL</code> må settes.
