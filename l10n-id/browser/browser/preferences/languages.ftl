# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Bahasa
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Terkadang laman web ditawarkan dalam beberapa bahasa. Pilih bahasa untuk menampilkan laman web ini, urut sesuai kesukaan
languages-customize-spoof-english =
    .label = Minta versi bahasa Inggris dari laman web untuk meningkatkan privasi
languages-customize-moveup =
    .label = Geser Atas
    .accesskey = A
languages-customize-movedown =
    .label = Geser Bawah
    .accesskey = B
languages-customize-remove =
    .label = Hapus
    .accesskey = H
languages-customize-select-language =
    .placeholder = Pilih bahasa untuk ditambah…
languages-customize-add =
    .label = Tambah
    .accesskey = T
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
