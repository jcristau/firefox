# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Laporan Mogok
clear-all-reports-label = Hapus Semua Laporan
delete-confirm-title = Yakin?
delete-confirm-description = Proses berikut akan menghapus semua laporan dan tidak dapat dikembalikan.
crashes-unsubmitted-label = Laporan Mogok yang Belum Dikirim
id-heading = ID Laporan
date-crashed-heading = Tanggal Mogok
crashes-submitted-label = Laporan Mogok yang Sudah Dikirim
date-submitted-heading = Tanggal Dikirim
no-reports-label = Tidak ada laporan kerusakan yang pernah dikirim.
no-config-label = Aplikasi belum diatur untuk menampilkan laporan kerusakan. Pengaturan <code>breakpad.reportURL</code> harus disetel terlebih dahulu.
