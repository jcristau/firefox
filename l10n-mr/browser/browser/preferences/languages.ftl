# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = भाषा
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = वेब पृष्ठे काही वेळा अनेक भाषांमध्ये देऊ केले जातात. प्राधान्यतेनुसार, वेब पृष्ठांच्या प्रदर्शनासाठी भाषा निवडा
languages-customize-spoof-english =
    .label = सुधारित सुरक्षेसाठी संकेतस्थळांच्या इंग्रजी आवृत्तीची मागणी करा
languages-customize-moveup =
    .label = वर हलवा
    .accesskey = U
languages-customize-movedown =
    .label = खाली हलवा
    .accesskey = D
languages-customize-remove =
    .label = काढुन टाका
    .accesskey = R
languages-customize-select-language =
    .placeholder = जमा करण्यासाठी भाषा निवडा…
languages-customize-add =
    .label = समाविष्ट करा
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
