# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = क्रॅश अहवाल
clear-all-reports-label = सर्व अहवाल काढून टाका
delete-confirm-title = आपण नक्की आहात?
delete-confirm-description = यामुळे सर्व अहवाल काढूण टाकले जातिल व पुन्ह प्राप्त करण्याजोगी राहणार नाही.
crashes-unsubmitted-label = न पाठविलेले क्रॅश अहवाल
id-heading = अहवाल ID
date-crashed-heading = क्रॅश झाल्याचा दिनांक
crashes-submitted-label = क्रॅश अहवाल पाठविला आहे
date-submitted-heading = सादर केल्याचा दिनांक
no-reports-label = क्रॅश अहवाल सादर केले गेले नाही.
no-config-label = हा अनुप्रोयग क्रॅश अहवाल दर्शविण्याकरीता संयोजीत केले गेले नाही. प्राधान्यता <code>breakpad.reportURL</code> निश्चित असावी.
