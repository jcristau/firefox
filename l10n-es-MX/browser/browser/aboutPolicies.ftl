# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Políticas Empresariales
# 'Active' is used to describe the policies that are currently active
active-policies-tab = Activas
errors-tab = Errores
documentation-tab = Documentación
policy-name = Nombre de la política
policy-value = Valor de la política
policy-errors = Errores de la política
