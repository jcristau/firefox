# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Idiomas y codificación de caracteres
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Las páginas web a veces están disponibles en más de un idioma. Elige los idiomas que se muestran en las páginas web en órden de preferencia
languages-customize-spoof-english =
    .label = Solicitar versiones en inglés de páginas web para mayor privacidad
languages-customize-moveup =
    .label = Subir
    .accesskey = S
languages-customize-movedown =
    .label = Bajar
    .accesskey = B
languages-customize-remove =
    .label = Quitar
    .accesskey = Q
languages-customize-select-language =
    .placeholder = Idiomas…
languages-customize-add =
    .label = Agregar
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
