# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Reportes de fallos
clear-all-reports-label = Quitar todos los reportes
delete-confirm-title = ¿Estás seguro?
delete-confirm-description = Esta acción eliminará todos los informes y no puede deshacerse.
crashes-unsubmitted-label = Informes de fallos no enviados
id-heading = ID del reporte
date-crashed-heading = La fecha falló
crashes-submitted-label = Reportes de fallos enviados
date-submitted-heading = Fecha enviada
no-reports-label = No se han enviado reportes de fallos.
no-config-label = Esta aplicación no está configurada para mostrar los reportes de fallos. Debe establecer la preferencia <code>breakpad.reportURL</code>.
