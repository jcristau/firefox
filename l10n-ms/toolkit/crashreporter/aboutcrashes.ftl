# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Laporan Ranap
clear-all-reports-label = Buang Semua Laporan
delete-confirm-title = Adakah anda pasti?
delete-confirm-description = Semua laporan akan dihapuskan dan tidak boleh dibatalkan.
crashes-unsubmitted-label = Laporan Ranap Tidak Dihantar
id-heading = ID Laporan
date-crashed-heading = Tarikh Ranap
crashes-submitted-label = Laporan Ranap Dihantar
date-submitted-heading = Tarikh Dihantar
no-reports-label = Tiada laporan ranap yang telah dihantar.
no-config-label = Aplikasi ini belum ditetapkan untuk memaparkan laporan ranap. Keutamaan <code>breakpad.reportURL</code> mesti ditetapkan.
