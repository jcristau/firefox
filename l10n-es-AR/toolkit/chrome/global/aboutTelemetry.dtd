<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutTelemetry.pingDataSource "
Fuente de datos de ping:
">
<!ENTITY aboutTelemetry.showCurrentPingData "
Datos de ping actuales
">
<!ENTITY aboutTelemetry.showArchivedPingData "
Datos de ping archivados
">
<!ENTITY aboutTelemetry.showSubsessionData "
Mostrar datos de subsesión
">
<!ENTITY aboutTelemetry.choosePing "
Elegir ping:
">
<!ENTITY aboutTelemetry.archivePingType "
Tipo de ping
">
<!ENTITY aboutTelemetry.archivePingHeader "
Ping
">
<!ENTITY aboutTelemetry.optionGroupToday "
Hoy
">
<!ENTITY aboutTelemetry.optionGroupYesterday "
Ayer
">
<!ENTITY aboutTelemetry.optionGroupOlder "
Antiguo
">
<!ENTITY aboutTelemetry.payloadChoiceHeader "
  Payload
">
<!-- LOCALIZATION NOTE(aboutTelemetry.previousPing, aboutTelemetry.nextPing):
	These strings are displayed when selecting Archived pings, and they’re
	used to move to the next or previous ping. -->
<!ENTITY aboutTelemetry.previousPing "&lt;&lt;">
<!ENTITY aboutTelemetry.nextPing "&gt;&gt;">

<!ENTITY aboutTelemetry.pageTitle "Datos de Telemetry">
<!ENTITY aboutTelemetry.moreInformations "
¿Buscando más información?
">
<!ENTITY aboutTelemetry.firefoxDataDoc "
La <a>documentación de datos de Firefox</a> contiene guías sobre como trabajar con nuestras herramientas de datos.
">
<!ENTITY aboutTelemetry.telemetryClientDoc "
La <a>documentación del cliente Telemetry de Firefox</a> incluye definiciones para conceptos, documentación de API y referencias de datos.
">
<!ENTITY aboutTelemetry.telemetryDashboard "Los <a>tableros de Telemetría</a> le permiten visualizar los datos de Telemetría que recibe Mozilla.">

<!ENTITY aboutTelemetry.showInFirefoxJsonViewer "
Abrir en el visor de JSON
">

<!ENTITY aboutTelemetry.homeSection "Inicio">
<!ENTITY aboutTelemetry.generalDataSection "
  Datos generales
">
<!ENTITY aboutTelemetry.environmentDataSection "
  Datos de entorno
">
<!ENTITY aboutTelemetry.sessionInfoSection "
  Información de sesión
">
<!ENTITY aboutTelemetry.scalarsSection "
 Escalares">
<!ENTITY aboutTelemetry.keyedScalarsSection "
  
  Escalares con clave">
<!ENTITY aboutTelemetry.histogramsSection "
  Histogramas
">
<!ENTITY aboutTelemetry.keyedHistogramsSection "
  Histogramas con clave
">
<!ENTITY aboutTelemetry.eventsSection "
  Eventos
">
<!ENTITY aboutTelemetry.simpleMeasurementsSection "
  Medidas simples
">
<!ENTITY aboutTelemetry.telemetryLogSection "
  Registro de Telemetry
">
<!ENTITY aboutTelemetry.slowSqlSection "
  Sentencias SQL lentas
">
<!ENTITY aboutTelemetry.chromeHangsSection "
  Colgadas del navegador
">
<!ENTITY aboutTelemetry.addonDetailsSection "
  Detalles de complementos
">
<!ENTITY aboutTelemetry.capturedStacksSection "
  
  Pilas capturadas">
<!ENTITY aboutTelemetry.lateWritesSection "
  Últimas escrituras
">
<!ENTITY aboutTelemetry.rawPayloadSection "
Payload en bruto
">
<!ENTITY aboutTelemetry.raw "
JSON en bruto
">

<!ENTITY aboutTelemetry.fullSqlWarning "
  NOTE: Slow SQL debugging is enabled. Full SQL strings may be displayed below but they will not be submitted to Telemetry.
">
<!ENTITY aboutTelemetry.fetchStackSymbols "
  Obtener nombres de función para pilas
">
<!ENTITY aboutTelemetry.hideStackSymbols "
  Mostrar datos de pila en bruto
">
