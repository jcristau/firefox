# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Informes de fallos
clear-all-reports-label = Eliminar todos los informes
delete-confirm-title = ¿Está seguro?
delete-confirm-description = Ésto borrará todos sus informes y no puede deshacerse
crashes-unsubmitted-label = Informes de fallos no enviados
id-heading = ID de informe
date-crashed-heading = Fecha de fallo
crashes-submitted-label = Informes de fallos enviados
date-submitted-heading = Fecha de envío
no-reports-label = No se han enviado informes de problemas.
no-config-label = Esta aplicación no ha sido configurada para mostrar informes de problemas. La preferencia <code>breakpad.reportURL</code> debe establecerse.
