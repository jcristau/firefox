# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Справаздачы пра крах
clear-all-reports-label = Выдаліць усе справаздачы
delete-confirm-title = Вы ўпэўнены?
delete-confirm-description = Гэта выдаліць усе справаздачы. Дзеянне немагчыма адмяніць.
crashes-unsubmitted-label = Неадпраўленыя справаздачы пра крах
id-heading = ID справаздачы
date-crashed-heading = Дата краха
crashes-submitted-label = Адпраўленыя справаздачы пра крах
date-submitted-heading = Дата адпраўкі
no-reports-label = Няма пададзеных справаздачаў.
no-config-label = Гэтая праграма не наладжана адлюстроўваць справаздачы пра крах. Перавага <code>breakpad.reportURL</code> мусіць быць усталявана.
