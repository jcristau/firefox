# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Мовы
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Часам старонкі сеціва даступны на некалькіх мовах. Выберыце мовы для прагляду старонак сеціва, у парадку пераваг
languages-customize-spoof-english =
    .label = Запытваць англійскую версію вэб-старонак для лепшай прыватнасці
languages-customize-moveup =
    .label = Рухаць угору
    .accesskey = г
languages-customize-movedown =
    .label = Рухаць уніз
    .accesskey = н
languages-customize-remove =
    .label = Выдаліць
    .accesskey = ц
languages-customize-select-language =
    .placeholder = Выбраць мову для дадання…
languages-customize-add =
    .label = Дадаць
    .accesskey = Д
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
