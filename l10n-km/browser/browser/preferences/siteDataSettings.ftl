# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Settings

site-data-search-textbox =
    .placeholder = ស្វែងរក​វេបសាយ
    .accesskey = S
site-data-column-host =
    .label = វេបសាយ
site-data-column-storage =
    .label = កន្លែង​ផ្ទុក
site-data-remove-selected =
    .label = លុប​អ្វី​ដែល​បាន​ជ្រើស
    .accesskey = r
site-data-button-cancel =
    .label = បោះបង់
    .accesskey = C
site-data-button-save =
    .label = រក្សាទុក​ការ​ផ្លាស់ប្ដូរ
    .accesskey = a
# Variables:
#   $value (Number) - Value of the unit (for example: 4.6, 500)
#   $unit (String) - Name of the unit (for example: "bytes", "KB")
site-usage-pattern = { $value } { $unit }
site-data-remove-all =
    .label = លុប​ចេញ​ទាំងអស់
    .accesskey = e
site-data-remove-shown =
    .label = លុប​ចេញ​ទាំងអស់​ដែល​បាន​បង្ហាញ
    .accesskey = e

## Removing

site-data-removing-window =
    .title = { site-data-removing-header }
site-data-removing-dialog =
    .title = { site-data-removing-header }
    .buttonlabelaccept = លុប
