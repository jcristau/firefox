# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

clear-site-data-window =
    .title = សម្អាតទិន្នន័យ
    .style = width: 35em
clear-site-data-cancel =
    .label = បោះបង់
    .accesskey = C
clear-site-data-clear =
    .label = សម្អាត
    .accesskey = l
