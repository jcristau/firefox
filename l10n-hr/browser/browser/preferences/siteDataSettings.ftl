# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Settings

site-data-search-textbox =
    .placeholder = Pretraži web stranice
    .accesskey = s
site-data-column-host =
    .label = Stranica
site-data-column-storage =
    .label = Pohrana
site-data-remove-selected =
    .label = Ukloni odabrano
    .accesskey = r
site-data-button-cancel =
    .label = Odustani
    .accesskey = O
site-data-button-save =
    .label = Spremi izmjene
    .accesskey = S
# Variables:
#   $value (Number) - Value of the unit (for example: 4.6, 500)
#   $unit (String) - Name of the unit (for example: "bytes", "KB")
site-usage-pattern = { $value } { $unit }
site-data-remove-all =
    .label = Ukloni sve
    .accesskey = e
site-data-remove-shown =
    .label = Ukloni sve prikazane
    .accesskey = e

## Removing

site-data-removing-window =
    .title = { site-data-removing-header }
site-data-removing-dialog =
    .title = { site-data-removing-header }
    .buttonlabelaccept = Ukloni
