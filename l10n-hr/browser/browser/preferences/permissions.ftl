# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = Iznimke
    .style = width: 41em
permissions-close-key =
    .key = w
permissions-address = Adresa web stranice
    .accesskey = d
permissions-block =
    .label = Blokiraj
    .accesskey = B
permissions-session =
    .label = Dopusti za sesiju
    .accesskey = s
permissions-allow =
    .label = Dopusti
    .accesskey = D
permissions-site-name =
    .label = Web stranica
permissions-status =
    .label = Stanje
permissions-remove =
    .label = Ukloni stranicu
    .accesskey = U
permissions-remove-all =
    .label = Ukloni sve stranice
    .accesskey = e
permissions-button-cancel =
    .label = Odustani
    .accesskey = O
permissions-button-ok =
    .label = Spremi promjene
    .accesskey = S
permissions-searchbox =
    .placeholder = Pretraži web stranicu
permissions-capabilities-allow =
    .label = Dopusti
permissions-capabilities-block =
    .label = Blokiraj
permissions-capabilities-prompt =
    .label = Uvijek pitaj

## Invalid Hostname Dialog

permissions-invalid-uri-title = Upisano ime poslužitelja je netočno
permissions-invalid-uri-label = Molim vas, upišite valjano ime poslužitelja

## Exceptions - Tracking Protection

permissions-exceptions-tracking-protection-window =
    .title = Iznimke - zaštita od praćenja
    .style = { permissions-window.style }
permissions-exceptions-tracking-protection-desc = Na ovim stranicama ste isključili zaštitu od praćenja.

## Exceptions - Cookies


## Exceptions - Pop-ups

permissions-exceptions-popup-window =
    .title = Dopuštene stranice - skočni prozori
    .style = { permissions-window.style }
permissions-exceptions-popup-desc = Možete odrediti koje web stranice smiju prikazivati skočne prozore. Upišite točnu adresu web stranice kojoj to želite dopustiti, te kliknite na Dopusti.

## Exceptions - Saved Logins

permissions-exceptions-saved-logins-window =
    .title = Iznimke - spremljene prijave
    .style = { permissions-window.style }
permissions-exceptions-saved-logins-desc = Prijave za sljedeće stranice neće biti spremljene

## Exceptions - Add-ons

permissions-exceptions-addons-window =
    .title = Dopuštene stranice - Instalacija dodataka
    .style = { permissions-window.style }
permissions-exceptions-addons-desc = Možete odabrati koje web stranice smiju instalirati dodatke. Upišite točnu adresu web stranice kojoj to želite dopustiti, te kliknite na Dopusti.

## Site Permissions - Notifications

permissions-site-notification-window =
    .title = Postavke - dozvole za obavijesti
    .style = { permissions-window.style }

## Site Permissions - Location

permissions-site-location-window =
    .title = Postavke - lokacijske dozvole
    .style = { permissions-window.style }

## Site Permissions - Camera

permissions-site-camera-window =
    .title = Postavke - dozvole pristupa kameri
    .style = { permissions-window.style }

## Site Permissions - Microphone

permissions-site-microphone-window =
    .title = Postavke - dozvole pristupa mikrofonu
    .style = { permissions-window.style }
