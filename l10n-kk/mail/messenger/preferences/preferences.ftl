# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = { -brand-short-name } мәзірі, хабарламалар және ескертулерді көрсетуге қолданылатын тілді таңдаңыз.
confirm-messenger-language-change-description = Бұл өзгерістерді іске асыру үшін, { -brand-short-name } қайта іске қосыңыз
confirm-messenger-language-change-button = Іске асыру және қайта қосу
