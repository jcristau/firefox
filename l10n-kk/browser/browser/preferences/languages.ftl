# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Тілдер
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Кейбір веб-сайттар бірнеше тілдерде болуы мүмкін. Тілдердің ретін таңдаңыз
languages-customize-spoof-english =
    .label = Жақсырақ қауіпсіздік үшін веб сайттардың ағылшын нұсқаларын сұрау
languages-customize-moveup =
    .label = Жоғары
    .accesskey = Ж
languages-customize-movedown =
    .label = Төмен
    .accesskey = Т
languages-customize-remove =
    .label = Өшіру
    .accesskey = ш
languages-customize-select-language =
    .placeholder = Қосылатын тілді таңдау…
languages-customize-add =
    .label = Қосу
    .accesskey = о
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale } [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
