# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Құлау хабарламалары
clear-all-reports-label = Барлық хабарлауларды өшіру
delete-confirm-title = Сіз шынымен осыны қалайсыз ба?
delete-confirm-description = Бұл әрекеттің нәтижесінде барлық хабарлар қайтымсыз жойылады.
crashes-unsubmitted-label = Құлау туралы жіберілмеген хабарламалар
id-heading = Хабар идентификаторы
date-crashed-heading = Құлаған күні
crashes-submitted-label = Құлау туралы жіберілген хабарлар
date-submitted-heading = Жіберу күні
no-reports-label = Құлау туралы ешбір хабар жіберілмеген.
no-config-label = Бағдарлама құлау туралы хабарларға бапталмаған. <code>breakpad.reportURL</code> параметрін қою керек.
