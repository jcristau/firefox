# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (saveAddressesMessage): %S is brandShortName. This string is used on the doorhanger to
# notify users that addresses are saved.
saveAddressesMessage = %S tallentaa nyt osoitteet, jotta voit täyttää lomakkeet nopeammin.
# LOCALIZATION NOTE (autofillOptionsLink, autofillOptionsLinkOSX): These strings are used in the doorhanger for
# updating addresses. The link leads users to Form Autofill browser preferences.
autofillOptionsLink = Lomakkeiden automaattitäytön asetukset
autofillOptionsLinkOSX = Lomakkeiden automaattitäytön asetukset
# LOCALIZATION NOTE (autofillSecurityOptionsLink, autofillSecurityOptionsLinkOSX): These strings are used
# in the doorhanger for saving credit card info. The link leads users to Form Autofill browser preferences.
autofillSecurityOptionsLink = Lomakkeiden automaattitäytön ja turvallisuuden asetukset
autofillSecurityOptionsLinkOSX = Lomakkeiden automaattitäytön ja turvallisuuden asetukset
# LOCALIZATION NOTE (changeAutofillOptions, changeAutofillOptionsOSX): These strings are used on the doorhanger
# that notifies users that addresses are saved. The button leads users to Form Autofill browser preferences.
changeAutofillOptions = Muuta lomakkeiden automaattitäytön asetuksia
changeAutofillOptionsOSX = Muuta lomakkeiden automaattitäytön asetuksia
changeAutofillOptionsAccessKey = M
# LOCALIZATION NOTE (addressesSyncCheckbox): If Sync is enabled, this checkbox is displayed on the doorhanger
# shown when saving addresses.
addressesSyncCheckbox = Jaa osoitteet synkronoitujen laitteiden kanssa
# LOCALIZATION NOTE (creditCardsSyncCheckbox): If Sync is enabled and credit card sync is available,
# this checkbox is displayed on the doorhanger shown when saving credit card.
creditCardsSyncCheckbox = Jaa luottokortit synkronoitujen laitteiden kanssa
# LOCALIZATION NOTE (updateAddressMessage, updateAddressDescriptionLabel, createAddressLabel, updateAddressLabel):
# Used on the doorhanger when an address change is detected.
updateAddressMessage = Haluatko päivittää osoitteen näillä uusilla tiedoilla?
updateAddressDescriptionLabel = Päivitettävä osoite:
createAddressLabel = Luo uusi osoite
createAddressAccessKey = L
updateAddressLabel = Päivitä osoite
updateAddressAccessKey = P
# LOCALIZATION NOTE (saveCreditCardMessage, saveCreditCardDescriptionLabel, saveCreditCardLabel, cancelCreditCardLabel, neverSaveCreditCardLabel):
# Used on the doorhanger when users submit payment with credit card.
# LOCALIZATION NOTE (saveCreditCardMessage): %S is brandShortName.
saveCreditCardMessage = Haluatko, että %S tallentaa tämän luottokortin? (Turvakoodia ei tallenneta)
saveCreditCardDescriptionLabel = Tallennettava luottokortti:
saveCreditCardLabel = Tallenna luottokortti
saveCreditCardAccessKey = T
cancelCreditCardLabel = Älä tallenna
cancelCreditCardAccessKey = a
neverSaveCreditCardLabel = Älä tallenna luottokortteja koskaan
neverSaveCreditCardAccessKey = e
# LOCALIZATION NOTE (updateCreditCardMessage, updateCreditCardDescriptionLabel, createCreditCardLabel, updateCreditCardLabel):
# Used on the doorhanger when an credit card change is detected.
updateCreditCardMessage = Haluatko päivittää luottokortin näillä uusilla tiedoilla?
updateCreditCardDescriptionLabel = Päivitettävä luottokortti:
createCreditCardLabel = Luo uusi luottokortti
createCreditCardAccessKey = L
updateCreditCardLabel = Päivitä luottokortti
updateCreditCardAccessKey = P
# LOCALIZATION NOTE (openAutofillMessagePanel): Tooltip label for Form Autofill doorhanger icon on address bar.
openAutofillMessagePanel = Avaa lomakkeiden automaattitäytön viestipaneeli

# LOCALIZATION NOTE ( (autocompleteFooterOptionShort, autocompleteFooterOptionOSXShort): Used as a label for the button,
# displayed at the bottom of the dropdown suggestion, to open Form Autofill browser preferences.
autocompleteFooterOptionShort = Asetukset
autocompleteFooterOptionOSXShort = Asetukset
# LOCALIZATION NOTE (category.address, category.name, category.organization2, category.tel, category.email):
# Used in autofill drop down suggestion to indicate what other categories Form Autofill will attempt to fill.
category.address = osoite
category.name = nimi
category.organization2 = organisaatio
category.tel = puhelin
category.email = sähköposti
# LOCALIZATION NOTE (fieldNameSeparator): This is used as a separator between categories.
fieldNameSeparator = ,\u0020
# LOCALIZATION NOTE (phishingWarningMessage, phishingWarningMessage2): The warning
# text that is displayed for informing users what categories are about to be filled.
# "%S" will be replaced with a list generated from the pre-defined categories.
# The text would be e.g. Also autofills organization, phone, email.
phishingWarningMessage = Täyttää automaattisesti myös: %S
phishingWarningMessage2 = Täyttää automaattisesti: %S
# LOCALIZATION NOTE (insecureFieldWarningDescription): %S is brandShortName. This string is used in drop down
# suggestion when users try to autofill credit card on an insecure website (without https).
insecureFieldWarningDescription = %S on havainnut suojaamattoman sivuston. Lomakkeiden automaattitäyttö on väliaikaisesti pois käytöstä.
# LOCALIZATION NOTE (clearFormBtnLabel2): Label for the button in the dropdown menu that used to clear the populated
# form.
clearFormBtnLabel2 = Tyhjennä automaattitäytetty lomake

# LOCALIZATION NOTE (autofillAddressesCheckbox): Label for the checkbox that enables autofilling addresses.
autofillAddressesCheckbox = Täytä osoitteet automaattisesti
# LOCALIZATION NOTE (learnMoreLabel): Label for the link that leads users to the Form Autofill SUMO page.
learnMoreLabel = Lue lisää
# LOCALIZATION NOTE (savedAddressesBtnLabel): Label for the button that opens a dialog that shows the
# list of saved addresses.
savedAddressesBtnLabel = Tallennetut osoitteet…
# LOCALIZATION NOTE (autofillCreditCardsCheckbox): Label for the checkbox that enables autofilling credit cards.
autofillCreditCardsCheckbox = Täytä luottokortit automaattisesti
# LOCALIZATION NOTE (savedCreditCardsBtnLabel): Label for the button that opens a dialog that shows the list
# of saved credit cards.
savedCreditCardsBtnLabel = Tallennetut luottokortit…

# LOCALIZATION NOTE (manageAddressesTitle, manageCreditCardsTitle): The dialog title for the list of addresses or
# credit cards in browser preferences.
manageAddressesTitle = Tallennetut osoitteet
manageCreditCardsTitle = Tallennetut luottokortit
# LOCALIZATION NOTE (addressesListHeader, creditCardsListHeader): The header for the list of addresses or credit cards
# in browser preferences.
addressesListHeader = Osoitteet
creditCardsListHeader = Luottokortit
showCreditCardsBtnLabel = Näytä luottokortit
hideCreditCardsBtnLabel = Piilota luottokortit
removeBtnLabel = Poista
addBtnLabel = Lisää…
editBtnLabel = Muokkaa…
# LOCALIZATION NOTE (manageDialogsWidth): This strings sets the default width for windows used to manage addresses and
# credit cards.
manageDialogsWidth = 560px

# LOCALIZATION NOTE (addNewAddressTitle, editAddressTitle): The dialog title for creating or editing addresses
# in browser preferences.
addNewAddressTitle = Lisää uusi osoite
editAddressTitle = Muokkaa osoitetta
givenName = Etunimi
additionalName = Toinen nimi
familyName = Sukunimi
organization2 = Organisaatio
streetAddress = Katuosoite
city = Kaupunki
province = Maakunta
state = Osavaltio
postalCode = Postinumero
zip = Postinumero
country = Maa tai alue
tel = Puhelin
email = Sähköposti
cancelBtnLabel = Peruuta
saveBtnLabel = Tallenna
countryWarningMessage2 = Lomakkeiden automaattitäyttö on tällä hetkellä käytettävissä vain joillekin maille.

# LOCALIZATION NOTE (addNewCreditCardTitle, editCreditCardTitle): The dialog title for creating or editing
# credit cards in browser preferences.
addNewCreditCardTitle = Lisää uusi luottokortti
editCreditCardTitle = Muokkaa luottokorttia
cardNumber = Luottokortti
invalidCardNumber = Kirjoita kelvollinen luottokortin numero
nameOnCard = Nimi kortissa
cardExpires = Vanhenee
billingAddress = Laskutusosoite
