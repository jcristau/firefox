# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Звіти про збої
clear-all-reports-label = Вилучити всі звіти
delete-confirm-title = Ви певні?
delete-confirm-description = Ця дія знищить назавжди всі ваші звіти про аварії
crashes-unsubmitted-label = Не надіслані звіти про збої
id-heading = ID звіту
date-crashed-heading = Дата збою
crashes-submitted-label = Надіслані звіти про збої
date-submitted-heading = Дата надсилання
no-reports-label = Жодних звітів про збої не надсилалось.
no-config-label = Ця програма не була налаштована для показу звітів про збої. Повинно бути встановлено налаштування <code>breakpad.reportURL</code>.
