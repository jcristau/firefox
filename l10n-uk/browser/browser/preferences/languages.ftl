# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Мови
    .style = width: 38em
languages-close-key =
    .key = w
languages-description = Деякі веб-сторінки можуть бути доступними більш ніж однією мовою. Оберіть мови, якими показувати такі сторінки, в порядку зручності
languages-customize-spoof-english =
    .label = Запитувати англійську версію веб-сайтів для покращеної приватності
languages-customize-moveup =
    .label = Вгору
    .accesskey = г
languages-customize-movedown =
    .label = Вниз
    .accesskey = з
languages-customize-remove =
    .label = Вилучити
    .accesskey = и
languages-customize-select-language =
    .placeholder = Додати мову…
languages-customize-add =
    .label = Додати
    .accesskey = о
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
