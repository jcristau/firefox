# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Poročila o sesutjih
clear-all-reports-label = Odstrani vsa poročila
delete-confirm-title = Ali ste prepričani?
delete-confirm-description = S tem boste izbrisali vsa poročila, česar ni mogoče razveljaviti.
crashes-unsubmitted-label = Neposlana poročila o sesutjih
id-heading = ID poročila
date-crashed-heading = Datum sesutja
crashes-submitted-label = Poslana poročila o sesutjih
date-submitted-heading = Datum pošiljanja
no-reports-label = Nobeno poročilo o sesutju ni bilo poslano.
no-config-label = Ta program ni bil nastavljen za prikaz poročil o sesutjih. Nastaviti je potrebno <code>breakpad.reportURL</code>.
