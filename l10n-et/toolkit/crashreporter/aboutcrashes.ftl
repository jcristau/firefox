# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Vearaportid
clear-all-reports-label = Eemalda kõik raportid
delete-confirm-title = Kas oled kindel?
delete-confirm-description = Jätkamisel kustutatakse kõik raportid ja seda pole võimalik tagasi võtta.
crashes-unsubmitted-label = Saatmata vearaportid
id-heading = Raporti ID
date-crashed-heading = Vea esinemise kuupäev
crashes-submitted-label = Saadetud vearaportid
date-submitted-heading = Vearaporti saatmise kuupäev
no-reports-label = Ühtegi vearaportit pole saadetud.
no-config-label = See rakendus pole seadistatud kuvama vearaporteid. Eelistus <code>breakpad.reportURL</code> peab olema määratud.
