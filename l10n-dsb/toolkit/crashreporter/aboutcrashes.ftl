# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Wowaleńske rozpšawy
clear-all-reports-label = Wšykne rozpšawy wótpóraś
delete-confirm-title = Sćo wěsty?
delete-confirm-description = To buźo wšykne rozpšawy lašowaś a njedajo se anulěrowaś.
crashes-unsubmitted-label = Njewótpósłane wowaleńske rozpšawy
id-heading = ID rozpšawy
date-crashed-heading = Datum wowalenja
crashes-submitted-label = Rozpósłane wowaleńske rozpšawy
date-submitted-heading = Datum rozpósłanja
no-reports-label = Rozpšawy wó wowalenjach njejsu se rozpósłali.
no-config-label = Toś to nałoženje njejo se konfigurěrowało, aby rozpšawy wó wowalenjach zwobrazniło. Nastajenje <code>breakpad.reportURL</code> musy se stajiś.
