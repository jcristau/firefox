# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Rěcy
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Webboki stoje wótergi we wěcej nježli jadnej rěcy k dispoziciji. Wubjeŕśo rěcy za pśedstajanje webbokow w swójom preferěrowanem pórěźe
languages-customize-spoof-english =
    .label = Engelskorěcne wersije webbokow za pólěpšonu priwatnosć pominaś
languages-customize-moveup =
    .label = Górjej
    .accesskey = G
languages-customize-movedown =
    .label = Dołoj
    .accesskey = D
languages-customize-remove =
    .label = Wótpóraś
    .accesskey = W
languages-customize-select-language =
    .placeholder = Rěc wubraś…
languages-customize-add =
    .label = Pśidaś
    .accesskey = P
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
