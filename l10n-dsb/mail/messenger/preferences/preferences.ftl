# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = Wubjeŕśo rěcy, kótarež se wužywaju, aby menije, powěsći a powěźeńki z { -brand-short-name } pokazali.
confirm-messenger-language-change-description = Startujśo { -brand-short-name } znowego, aby toś te změny nałožył
confirm-messenger-language-change-button = Nałožyś a znowego startowaś
