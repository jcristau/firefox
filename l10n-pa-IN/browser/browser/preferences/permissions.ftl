# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = ਛੋਟ
    .style = width: 36em
permissions-close-key =
    .key = w
permissions-address = ਵੈੱਬਸਾਈਟ ਦਾ ਸਿਰਨਾਵਾਂ
    .accesskey = d
permissions-block =
    .label = ਪਾਬੰਦੀ
    .accesskey = B
permissions-session =
    .label = ਇਸ ਸ਼ੈਸ਼ਨ ਲਈ
    .accesskey = S
permissions-allow =
    .label = ਮਨਜ਼ੂਰ ਕਰੋ
    .accesskey = A
permissions-site-name =
    .label = ਵੈੱਬਸਾਈਟ
permissions-status =
    .label = ਹਾਲਤ
permissions-remove =
    .label = ਵੈੱਬਸਾਈਟ ਨੂੰ ਹਟਾਉ
    .accesskey = R
permissions-remove-all =
    .label = ਸਾਰੀਆਂ ਵੈੱਬਸਾਈਟਾਂ ਨੂੰ ਹਟਾਉ
    .accesskey = e
permissions-button-cancel =
    .label = ਰੱਦ ਕਰੋ
    .accesskey = C
permissions-button-ok =
    .label = ਤਬਦੀਲੀਆਂ ਨੂੰ ਸੰਭਾਲੋ
    .accesskey = S
permissions-searchbox =
    .placeholder = ਵੈੱਬਾਸਾਈਟ ਨੂੰ ਲੱਭੋ
permissions-capabilities-allow =
    .label = ਮਨਜ਼ੂਰ
permissions-capabilities-block =
    .label = ਪਾਬੰਦੀ
permissions-capabilities-prompt =
    .label = ਹਮੇਸ਼ਾਂ ਪੁੱਛੋ

## Invalid Hostname Dialog

permissions-invalid-uri-title = ਗਲਤ ਹੋਸਟ ਨਾਂ ਦਿੱਤਾ ਗਿਆ
permissions-invalid-uri-label = ਠੀਕ ਹੋਸਟ ਨਾਂ ਦਿਓ ਜੀ

## Exceptions - Tracking Protection

permissions-exceptions-tracking-protection-window =
    .title = ਛੋਟਾਂ - ਟਰੈਕਿੰਗ ਤੋਂ ਸੁਰੱਖਿਆ
    .style = { permissions-window.style }
permissions-exceptions-tracking-protection-desc = ਤੁਸੀਂ ਇਹਨਾਂ ਵੈੱਬਸਾਈਟਾਂ ਲਈ ਟਰੈਕਿੰਗ ਸੁਰੱਖਿਆ ਨੂੰ ਅਸਮਰੱਥ ਕਰ ਚੁੱਕੇ ਹੋ।

## Exceptions - Cookies

permissions-exceptions-cookie-window =
    .title = ਛੋਟਾਂ - ਕੂਕੀਜ਼ ਅਤੇ ਸਾਈਟ ਡਾਟਾ
    .style = { permissions-window.style }

## Exceptions - Pop-ups

permissions-exceptions-popup-window =
    .title = ਇਜਾਜ਼ਤ ਦਿੱਤੀਆਂ ਵੈੱਬਸਾਈਟਾਂ - ਪੋਪਅੱਪ
    .style = { permissions-window.style }
permissions-exceptions-popup-desc = ਤੁਸੀਂ ਇਹ ਤਹਿ ਕਰ ਸਕਦੇ ਹੋ ਕਿ ਕਿਹੜੀਆਂ ਵੈਬ ਸਾਇਟਾਂ ਨੂੰ ਪੋਪਅੱਪ ਵਿੰਡੋਜ਼ ਨੂੰ ਲੋਡ ਕਰ ਸਕਦੀਆਂ ਹਨ। ਸਾਇਟ ਦਾ ਸਹੀਂ ਐਡਰੈੱਸ ਦਿਓ ਅਤੇ ਫਿਰ ਸਵੀਕਾਰ ਜਾਂ ਪਾਬੰਦੀ ਦਬਾਓ।

## Exceptions - Saved Logins

permissions-exceptions-saved-logins-window =
    .title = ਛੋਟਾਂ - ਸੰਭਾਲੇ ਲਾਗਇਨ
    .style = { permissions-window.style }
permissions-exceptions-saved-logins-desc = ਅੱਗੇ ਦਿੱਤੀਆਂ ਵੈੱਬਸਾਈਟਾਂ ਲਈ ਲਾਗਇਨਾਂ ਨੂੰ ਸੰਭਾਲਿਆ ਨਹੀਂ ਜਾਵੇਗਾ

## Exceptions - Add-ons

permissions-exceptions-addons-window =
    .title = ਇਜਾਜ਼ਤ ਦਿੱਤੀਆਂ ਵੈੱਬਸਾਈਟਾਂ - ਐਨ-ਆਨ ਇੰਸਟਾਲੇਸ਼ਨ
    .style = { permissions-window.style }
permissions-exceptions-addons-desc = ਤੁਸੀਂ ਦੱਸ ਸਕਦੇ ਹੋ ਕਿ ਕਿਹੜੀਆਂ ਵੈੱਬ ਸਾਇਟਾਂ ਤੋਂ ਐਂਡ-ਆਨ ਇੰਸਟਾਲ ਕਰਨ ਦੀ ਇਜ਼ਾਜਤ ਹੈ। ਉਸ ਵੈੱਬ ਸਾਇਟ ਦੀ ਠੀਕ ਐਡਰੈੱਸ ਦਿਓ, ਜਿਸ ਨੂੰ ਤੁਸੀਂ ਇਜ਼ਾਜਤ ਦਿੰਦੇ ਹੋ, ਅਤੇ ਇਜ਼ਾਜਤ ਹੈ ਨੂੰ ਦਬਾਓ।

## Exceptions - Autoplay Media

permissions-exceptions-autoplay-media-window =
    .title = ਮਨਜ਼ੂਰੀ ਦਿੱਤੀਆਂ ਵੈੱਬਸਾਈਟਾਂ - ਆਪਣੇ-ਆਪ ਚਲਾਓ
    .style = { permissions-window.style }

## Site Permissions - Notifications

permissions-site-notification-window =
    .title = ਸੈਟਿੰਗਾਂ - ਨੋਟੀਫਿਕੇਸ਼ਨ ਇਜਾਜ਼ਤਾਂ
    .style = { permissions-window.style }
permissions-site-notification-disable-label =
    .label = ਸੂਚਨਾਵਾਂ ਵਾਸਤੇ ਇਜਾਜ਼ਤ ਲਈ ਪੁੱਛਣ ਲਈ ਨਵੀਆਂ ਬੇਨਤੀਆਂ ਉੱਤੇ ਰੋਕ ਲਗਾਓ

## Site Permissions - Location

permissions-site-location-window =
    .title = ਸੈਟਿੰਗਾਂ - ਟਿਕਾਣਾ ਇਜਾਜ਼ਤਾਂ
    .style = { permissions-window.style }
permissions-site-location-disable-label =
    .label = ਤੁਹਾਡੇ ਟਿਕਾਣੇ ਲਈ ਪਹੁੰਚ ਵਾਸਤੇ ਪੁੱਛਣ ਦੀਆਂ ਨਵੀਆਂ ਬੇਨਤੀਆਂ ਉੱਤੇ ਰੋਕ ਲਗਾਓ

## Site Permissions - Camera

permissions-site-camera-window =
    .title = ਸੈਟਿੰਗਾਂ - ਕੈਮਰਾ ਇਜਾਜ਼ਤਾਂ
    .style = { permissions-window.style }
permissions-site-camera-disable-label =
    .label = ਤੁਹਾਡੇ ਕੈਮਰੇ ਲਈ ਪਹੁੰਚ ਲਈ ਪੁੱਛਣ ਦੀਆਂ ਨਵੀਆਂ ਬੇਨਤੀਆਂ ਉੱਤੇ ਰੋਕ ਲਗਾਓ

## Site Permissions - Microphone

permissions-site-microphone-window =
    .title = ਸੈਟਿੰਗਾਂ - ਮਾਈਕਰੋਫ਼ੋਨ ਇਜਾਜ਼ਤਾਂ
    .style = { permissions-window.style }
permissions-site-microphone-disable-label =
    .label = ਤੁਹਾਡੇ ਮਾਈਕਰੋਫ਼ੋਨ ਲਈ ਪਹੁੰਚ ਵਾਸਤੇ ਪੁੱਛਣ ਦੀਆਂ ਨਵੀਆਂ ਬੇਨਤੀਆਂ ਉੱਤੇ ਰੋਕ ਲਗਾਓ
