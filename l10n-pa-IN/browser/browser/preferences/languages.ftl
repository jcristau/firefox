# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = ਬੋਲੀਆਂ
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = ਵੈੱਬ ਵਰਕੇ ਕਈ ਵਾਰ ਇੱਕ ਤੋਂ ਵੱਧ ਬੋਲੀਆਂ ਵਿੱਚ ਪੇਸ਼ ਕੀਤੇ ਜਾਂਦੇ ਹਨ। ਇਹ ਵੈੱਬ ਵਰਕੇ ਦਿਖਾਉਣ ਲਈ ਤਰਜੀਹੀ ਕ੍ਰਮ ਮੁਤਾਬਕ ਬੋਲੀਆਂ ਚੁਣੋ।
languages-customize-spoof-english =
    .label = ਸੁਧਰੀ ਪਰਦੇਦਾਰੀ ਲਈ ਵੈੱਬ ਵਰਕਿਆਂ ਦੀ ਅੰਗਰੇਜ਼ੀ ਵੰਨਗੀ ਦੀ ਬੇਨਤੀ ਕਰੋ
languages-customize-moveup =
    .label = ਉੱਤੇ ਸਰਕਾਉ
    .accesskey = U
languages-customize-movedown =
    .label = ਹੇਠਾਂ ਸਰਕਾਉ
    .accesskey = D
languages-customize-remove =
    .label = ਹਟਾਉ
    .accesskey = R
languages-customize-select-language =
    .placeholder = ਜੋੜਨ ਲਈ ਬੋਲੀ ਚੁਣੋ...
languages-customize-add =
    .label = ਜੋੜੋ
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
