# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = דיווחי קריסה
clear-all-reports-label = הסרת כל הדיווחים
delete-confirm-title = האם אתה בטוח?
delete-confirm-description = פעולה זו תמחק את כל הדיווחים ולא ניתנת לביטול.
crashes-unsubmitted-label = דיווחי קריסה שלא נשלחו
id-heading = קוד דיווח
date-crashed-heading = תאריך הקריסה
crashes-submitted-label = דיווחי קריסה שנשלחו
date-submitted-heading = תאריך השליחה
no-reports-label = לא נשלחו דיווחי קריסה.
no-config-label = ישום זה לא מוגדר להציג דיווחי קריסות. המאפיין <code>breakpad.reportURL</code> חייב להיות מוגדר.
