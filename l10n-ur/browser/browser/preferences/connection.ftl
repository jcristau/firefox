# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-window =
    .title = کنکشن سیٹکگیں
    .style =
        { PLATFORM() ->
            [macos] width: 44em
           *[other] width: 49em
        }
connection-close-key =
    .key = w
connection-disable-extension =
    .label = توسیعات نا اہل بنائیں
connection-proxy-option-no =
    .label = کوئی پراکسی نہیں
    .accesskey = پ
connection-proxy-option-system =
    .label = نظام کی پراکسی سیٹنگیں استعمال کریں
    .accesskey = ا
connection-proxy-option-auto =
    .label = اس نیٹورک کے لیے پراکسی سیٹنگیں خود کھوج کریں
    .accesskey = ن
connection-proxy-http = HTTP پراکسی
    .accesskey = x
connection-proxy-http-port = پورٹ
    .accesskey = پ
connection-proxy-http-share =
    .label = تمام پروٹوکول کے لیے یہ پراکسی سرور استعمال کریں
    .accesskey = ا
connection-proxy-ssl = SSL پراکسی
    .accesskey = L
connection-proxy-ssl-port = پورٹ
    .accesskey = س
connection-proxy-ftp = FTP  پراکسی
    .accesskey = F
connection-proxy-ftp-port = پورٹ
    .accesskey = ف
connection-proxy-socks-port = پورٹ
    .accesskey = ٹ
connection-proxy-socks4 =
    .label = SOCKS ورژن 4
    .accesskey = K
connection-proxy-socks5 =
    .label = SOCKS ورژن 5
    .accesskey = و
connection-proxy-noproxy-desc = مثال: .mozilla.org ،.net.nz ،192.168.1.0/24
connection-proxy-autotype =
    .label = خودکار پراکسی تشکیل URL
    .accesskey = A
connection-proxy-reload =
    .label = پھر لوڈ کریں
    .accesskey = پ
connection-proxy-autologin =
    .label = تصدیق کے لئے فوری طور پر ترغیب نہیں دیں اگر پاس ورڈ پہلے سے محفوظ شدہ ہے
    .accesskey = غ
    .tooltip = یہ اختیار خاموشی سے پراکسی پر توثیق کر دیتی ہے جب آپ نے ان کے لیے اسناد محفوظ کیے ہوں۔  توثیق ناکام ہونے کی صورت میں آپ کو بتا دیا جائے گا۔
connection-proxy-socks-remote-dns =
    .label = پراکسی DNS جب استعمال کر رہے ہوں SOCKS v5
    .accesskey = d
