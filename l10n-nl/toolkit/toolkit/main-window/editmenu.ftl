### This file contains the entities needed for the 'edit' menu
### It's currently only used for the Browser Console

editmenu-undo =
    .label = Ongedaan maken
    .accesskey = O

editmenu-redo =
    .label = Opnieuw uitvoeren
    .accesskey = u

editmenu-cut =
    .label = Knippen
    .accesskey = n

editmenu-copy =
    .label = Kopiëren
    .accesskey = K

editmenu-paste =
    .label = Plakken
    .accesskey = P

editmenu-delete =
    .label = Verwijderen
    .accesskey = V

editmenu-select-all =
    .label = Alles selecteren
    .accesskey = A
