# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Crashrapporten

clear-all-reports-label = Alle rapporten verwijderen
delete-confirm-title = Weet u het zeker?
delete-confirm-description = Dit zal alle rapporten verwijderen en kan niet ongedaan worden gemaakt.

crashes-unsubmitted-label = Niet-verzonden crashrapporten
id-heading = Rapport-ID
date-crashed-heading = Crashdatum

crashes-submitted-label = Verzonden crashrapporten
date-submitted-heading = Verzenddatum

no-reports-label = Er zijn geen crashrapporten verzonden.
no-config-label = Deze toepassing is niet geconfigureerd om crashrapporten weer te geven. De voorkeursinstelling <code>breakpad.reportURL</code> moet worden ingesteld.
