# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Relatórios de travamentos
clear-all-reports-label = Remover todos os relatórios
delete-confirm-title = Tem certeza?
delete-confirm-description = Isto irá excluir todos os relatórios e não pode ser desfeito.
crashes-unsubmitted-label = Relatórios de travamentos não enviados
id-heading = ID do relatório
date-crashed-heading = Data do travamento
crashes-submitted-label = Relatórios de travamentos enviados
date-submitted-heading = Data de envio
no-reports-label = Nenhum relatório de travamento foi enviado.
no-config-label = Este aplicativo não foi configurado para exibir relatórios de travamento. A preferência <code>breakpad.reportURL</code> deve ser definida.
