# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Αναφορές κατάρρευσης
clear-all-reports-label = Αφαίρεση όλων των αναφορών
delete-confirm-title = Είστε σίγουρος;
delete-confirm-description = Αυτή η ενέργεια θα διαγράψει όλες τις αναφορές και δεν είναι αναστρέψιμη
crashes-unsubmitted-label = Μη υποβληθείσες αναφορές κατάρρευσης
id-heading = ID Αναφοράς
date-crashed-heading = Ημερομηνία κατάρρευσης
crashes-submitted-label = Υποβληθείσες αναφορές κατάρρευσης
date-submitted-heading = Ημερομηνία υποβολής
no-reports-label = Δεν έχουν υποβληθεί αναφορές προβλημάτων.
no-config-label = Αυτή η εφαρμογή δεν έχει ρυθμιστεί για να εμφανίζει αναφορές προβλημάτων. Θα πρέπει να ρυθμιστούν οι προτιμήσεις στο <code>breakpad.reportURL</code>.
