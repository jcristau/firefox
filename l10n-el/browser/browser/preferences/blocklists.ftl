# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Λίστες αποκλεισμού
    .style = width: 55em
blocklist-desc = Μπορείτε να επιλέξετε ποια λίστα θα χρησιμοποιεί ο { -brand-short-name } για να αποκλείει στοιχεία της σελίδας που μπορεί να καταγράφουν τη συμπεριφορά σας.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Λίστα
blocklist-button-cancel =
    .label = Ακύρωση
    .accesskey = Α
blocklist-button-ok =
    .label = Αποθήκευση αλλαγών
    .accesskey = θ
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Βασική προστασία Disconnect.me (Προτεινόμενη).
blocklist-item-moz-std-desc = Επιτρέπει ορισμένους ιχνηλάτες προκειμένου να λειτουργούν σωστά οι ιστοσελίδες.
blocklist-item-moz-full-name = Αυστηρή προστασία Disconnect.me.
blocklist-item-moz-full-desc = Αποκλεισμός γνωστών ιχνηλατών. Ορισμένες ιστοσελίδες ενδέχεται να μη λειτουργούν σωστά.
