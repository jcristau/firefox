# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Πολιτικές επιχειρήσεων
# 'Active' is used to describe the policies that are currently active
active-policies-tab = Ενεργές
errors-tab = Σφάλματα
documentation-tab = Τεκμηρίωση
policy-name = Όνομα πολιτικής
policy-value = Τιμή πολιτικής
policy-errors = Σφάλματα πολιτικής
