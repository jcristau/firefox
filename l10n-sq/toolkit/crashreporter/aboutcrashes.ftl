# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Njoftime Vithisjesh
clear-all-reports-label = Hiqi Krejt Njoftimet
delete-confirm-title = Jeni i sigurt?
delete-confirm-description = Kjo do të fshijë tërë njoftimet dhe nuk mund të zhbëhet
crashes-unsubmitted-label = Njoftime Vithisjesh të Paparashtruar
id-heading = ID Njoftimi
date-crashed-heading = Datë Vithisjeje
crashes-submitted-label = Njoftime Vithisjesh të Parashtruar
date-submitted-heading = Datë Parashtrimi
no-reports-label = Nuk ka njoftime të parashtruar.
no-config-label = Ky aplikacion nuk është formësuar për paraqitje njoftimesh vithisjesh. Duhet rregulluar parapëlqimi <code>breakpad.reportURL</code>.
