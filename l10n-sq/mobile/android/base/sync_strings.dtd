<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Lidhu me &syncBrand.shortName.label;-un'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Që të aktivizoni pajisjen tuaj të re, përzgjidhni te pajisja “Rregulloni &syncBrand.shortName.label;-un”.'>
<!ENTITY sync.subtitle.pair.label 'Që ta aktivizoni, përzgjidhni te pajisja juaj tjetër “Çiftoni një pajisje”.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Nuk e kam me vete pajisjen…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Kredenciale hyrjesh'>
<!ENTITY sync.configure.engines.title.history 'Historikun tuaj'>
<!ENTITY sync.configure.engines.title.tabs 'Skedat tuaja'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; në &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Menu Faqerojtësish'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Etiketa'>
<!ENTITY bookmarks.folder.toolbar.label 'Panel Faqerojtësish'>
<!ENTITY bookmarks.folder.other.label 'Faqerojtës të Tjerë'>
<!ENTITY bookmarks.folder.desktop.label 'Faqerojtës Desktopi'>
<!ENTITY bookmarks.folder.mobile.label 'Faqerojtës Celulari'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Të fiksuar'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Mbrapsht në shfletim'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Mirë se vini te &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Bëni hyrjen që të njëkohësoni skedat, faqerojtësit, kredencialet tuaja të hyrjeve &amp; etj.'>
<!ENTITY fxaccount_getting_started_get_started 'Fillojani'>
<!ENTITY fxaccount_getting_started_old_firefox 'Po përdorni një version të vjetër të &syncBrand.shortName.label;-ut'>

<!ENTITY fxaccount_status_auth_server 'Shërbyes llogarie'>
<!ENTITY fxaccount_status_sync_now 'Njëkohësoje tani'>
<!ENTITY fxaccount_status_syncing2 'Po njëkohësohet…'>
<!ENTITY fxaccount_status_device_name 'Emër pajisjeje'>
<!ENTITY fxaccount_status_sync_server 'Shërbyes Sync'>
<!ENTITY fxaccount_status_needs_verification2 'Llogaria juaj duhet verifikuar. Prekeni që të ridërgohet email-i i verifikimit.'>
<!ENTITY fxaccount_status_needs_credentials 'Nuk lidhet dot. Prekeni që të hyni.'>
<!ENTITY fxaccount_status_needs_upgrade 'Lypset të përmirësoni &brandShortName; që të hyni.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; është rregulluar, por nuk njëkohëson vetvetiu. Zgjidhni “Vetënjëkohësim të dhënash” te Rregullime Për Android &gt; Përdorim të Dhënash.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; është rregulluar, por nuk njëkohëson vetvetiu. Zgjidhni “Vetënjëkohësim të dhënash” te Rregullime Për Android &gt; Llogari.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Prekeni që të bëni hyrjen te Llogaria juaj e re Firefox.'>
<!ENTITY fxaccount_status_choose_what 'Zgjidhni çfarë të njëkohësohet'>
<!ENTITY fxaccount_status_bookmarks 'Faqerojtës'>
<!ENTITY fxaccount_status_history 'Historik'>
<!ENTITY fxaccount_status_passwords2 'Kredenciale hyrjesh'>
<!ENTITY fxaccount_status_tabs 'Skeda të hapura'>
<!ENTITY fxaccount_status_additional_settings 'Rregullime Shtesë'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Njëkohëso vetëm përmes Wi-Fi'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Parandloje &brandShortName;-i të njëkohësojë përmes një rrjeti celular apo me pagesë'>
<!ENTITY fxaccount_status_legal 'Ligjore' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Kushte shërbimi'>
<!ENTITY fxaccount_status_linkprivacy2 'Shënim mbi privatësinë'>
<!ENTITY fxaccount_remove_account 'Shkëputeni&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Të shkëputet prej Sync-u?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Të dhënat mbi shfletimin tuaj do të mbeten në këtë pajisje, por s’do të njëkohësohen më me llogarinë tuaj.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Llogaria Firefox &formatS; u shkëput.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Shkëputu'>

<!ENTITY fxaccount_enable_debug_mode 'Aktivizoni Mënyrën Diagnostikim'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title 'Mundësi &syncBrand.shortName.label;-u'>
<!ENTITY fxaccount_options_configure_title 'Formësoni &syncBrand.shortName.label;-un'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label;-u nuk është i lidhur'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Prekeni që të hyni si &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Të përfundohet përmirësimi i &syncBrand.shortName.label;-ut?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Prekeni që të hyni si &formatS;'>
