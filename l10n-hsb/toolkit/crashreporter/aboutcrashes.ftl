# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Rozprawy wo spadach
clear-all-reports-label = Wšě rozprawy wotstronić
delete-confirm-title = Sće wěsty?
delete-confirm-description = To zhaša wšě rozprawy a njeda so cofnyć.
crashes-unsubmitted-label = Njewotpósłane rozprawy wo spadach
id-heading = ID rozprawy
date-crashed-heading = Datum spada
crashes-submitted-label = Rozpósłane rozprawy wo spadach
date-submitted-heading = Datum rozpósłanja
no-reports-label = Rozprawy wo spadach njejsu so rozpósłali.
no-config-label = Tute nałoženje njeje so konfigurowało, zo by rozprawy wo spadach zwobrazniło. Nastajenje <code>breakpad.reportURL</code> dyrbi so stajić.
