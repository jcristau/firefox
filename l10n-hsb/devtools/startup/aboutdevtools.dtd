<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "Wo wuwiwarskich nastrojach">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Wuwiwarske nastroje Firefox zmóžnić">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "Wuwiwarske nastroje Firefox zmóžnić, zo by so Element přepytować wužiwało">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "Přepytajće a wobdźěłajće HTML a CSS z Inspektora wuwiwarskich nastrojow.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "Wuwiwajće a testujće WebExtensions, webworkery, service workery a wjace z wuwiwarskimi nastrojemi Firefox.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "Sće tastowu skrótšenku wuwiwarskich nastrojow aktiwizował. Jeli běše to zmylk, móžeće tutón rajtark začinić.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage
          "Přepytajće a wobdźěłajće HTML, CSS a JavaScript z nastrojemi kaž Inspektor a pytanje za zmylkami.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "Wudokonjejće HTML, CSS a JavaScript swojeho websydła z nastrojemi kaž Inspektor a pytanje za zmylkami.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "Wuwiwarske nastroje Firefox su znjemóžnjene po standardźe, zo byšće wjace kontrole nad swojim wobhladowakom měł.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "Zhońće wjace wo wuwiwarskich nastrojach">

<!ENTITY  aboutDevtools.enable.enableButton "Wuwiwarske nastroje zmóžnić">
<!ENTITY  aboutDevtools.enable.closeButton "Tutu stronu začinić">

<!ENTITY  aboutDevtools.enable.closeButton2 "Tutón rajtark začinić">

<!ENTITY  aboutDevtools.welcome.title "Witajće k wuwiwarskim nastrojam Firefox!">

<!ENTITY  aboutDevtools.newsletter.title "Wuwiwarski powěsćowy list Mozilla">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "Wobstarajće sej nowinki wuwiwarjow, pokiwy a resursy, kotrež so direktnje na was sćelu.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "E-mejl">
<!ENTITY  aboutDevtools.newsletter.privacy.label "Sym přezjedny, zo Mozilla po <a class='external' href='https://www.mozilla.org/privacy/'>tutych prawidłach priwatnosće</a> z mojimi informacijemi wobchadźa.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "Abonować">
<!ENTITY  aboutDevtools.newsletter.thanks.title "Wulki dźak!">
<!ENTITY  aboutDevtools.newsletter.thanks.message "Jeli hišće njejsće abonement powěsćoweho lista Mozilla wobkrućił, dyrbiće to nětko činić. Prošu hladajće do dochadneje e-mejle abo papjernika za e-mejlku wot nas.">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "Pytaće wjace hač jenož wuwiwarske nastroje? Wuslědźće wobhladowak Firefox, kotryž je so specielnje za wuwiwarjow a moderne dźěłowe wotběhi wuwił.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "Dalše informacije">

