# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Rěče
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Webstrony so druhdy we wjace hač jednej rěči k dispoziciji steja. Wubjerće rěče za předstajenje tutych webstronow we swojim preferowanym porjedźe
languages-customize-spoof-english =
    .label = Jendźelskorěčne wersije webstronow za polěpšenu priwatnosć požadać
languages-customize-moveup =
    .label = Horje
    .accesskey = H
languages-customize-movedown =
    .label = Dele
    .accesskey = D
languages-customize-remove =
    .label = Wotstronić
    .accesskey = W
languages-customize-select-language =
    .placeholder = Rěč wubrać…
languages-customize-add =
    .label = Přidać
    .accesskey = P
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
