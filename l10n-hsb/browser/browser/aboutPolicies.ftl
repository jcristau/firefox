# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Předewzaćelske prawidła
# 'Active' is used to describe the policies that are currently active
active-policies-tab = Aktiwny
errors-tab = Zmylki
documentation-tab = Dokumentacija
policy-name = Mjeno prawidła
policy-value = Hódnota prawidła
policy-errors = Zmylki prawidłow
