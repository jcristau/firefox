<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY % brandDTD SYSTEM "chrome://branding/locale/brand.dtd">
%brandDTD;

<!ENTITY loadError.label "Zmylk při čitanju strony">
<!ENTITY retry.label "Hišće raz spytać">
<!ENTITY returnToPreviousPage.label "Wróćo hić">
<!ENTITY returnToPreviousPage1.label "Wróćo (doporučeny)">
<!ENTITY advanced.label "Rozšěrjene">

<!ENTITY continue2.label "Wjace…">
<!ENTITY moreInformation.label "Dalše informacije">

<!-- Specific error messages -->

<!ENTITY connectionFailure.title "Zwisk móžny njeje">
<!ENTITY connectionFailure.longDesc "&sharedLongDesc;">

<!ENTITY deniedPortAccess.title "Tuta adresa je wobmjezowana">
<!ENTITY deniedPortAccess.longDesc "">

<!ENTITY dnsNotFound.pageTitle "Serwer njebu namakany">
<!-- Localization note (dnsNotFound.title1) - "Hmm" is a sound made when considering or puzzling over something. You don't have to include it in your translation if your language does not have a written word like this. -->
<!ENTITY dnsNotFound.title1 "Hm. Mamy problemy, te sydło namakać.">
<!ENTITY dnsNotFound.longDesc1 "<strong>Jeli ta adresa je korektna, móžeće:</strong><ul>
  <li>Pozdźišo hišće raz spytać.</li>
  <li>Syćowy zwisk přepruwować.</li>
  <li>Jeli sće zwjazany, sće wšak za wohnjowej murju, přepruwujće, hač &brandShortName; ma prawo na přistup na web.</li></ul>">

<!ENTITY fileNotFound.title "Dataja njebu namakana">
<!ENTITY fileNotFound.longDesc "<ul> <li>Přepruwujće datajowe mjeno za wulkopisanskimi abo hinašimi pisanskimi zmylkami.</li> <li>Přepruwujće, hač je so dataja přesunyła, přemjenowała abo wotstroniła.</li> </ul>">

<!ENTITY fileAccessDenied.title "Přistup na dataju je so wotpokazał">
<!ENTITY fileAccessDenied.longDesc "<ul><li>Snano je so wotstroniła, přesunyła, abo datajowe prawa zadźěwaju přistupej.</li></ul>">

<!ENTITY generic.title "Hopla.">
<!ENTITY generic.longDesc "<p>&brandShortName; njemóže tutu stronu z někajkeje přičiny začitać.</p>">

<!ENTITY captivePortal.title "So pola syće přizjewić">
<!ENTITY captivePortal.longDesc2 "<p>Dyrbiće so pola tuteje syće přizjewić, prjedy hač móžeće přistup na internet měć.</p>">

<!ENTITY openPortalLoginPage.label2 "Přizjewjensku stronu syće wočinić">

<!ENTITY malformedURI.pageTitle "Njepłaćiwy URL">
<!-- Localization note (malformedURI.title1) - "Hmm" is a sound made when considering or puzzling over something. You don't have to include it in your translation if your language does not have a written word like this. -->
<!ENTITY malformedURI.title1 "Hm. Zda so, zo tuta adresa korektna njeje.">

<!ENTITY netInterrupt.title "Zwisk bu přetorhnjeny">
<!ENTITY netInterrupt.longDesc "&sharedLongDesc;">

<!ENTITY notCached.title "Dokument je njepłaćiwy">
<!ENTITY notCached.longDesc "<p>Požadany dokument w pufrowaku &brandShortName; k dispoziciji njesteji.</p><ul><li>Jako wěstotnu naprawu &brandShortName; sensibelne dokumenty awtomatisce znowa njewotwołuje.</li><li>Klikńće na Hišće raz, zo byšće dokument hišće raz z websydła wotwołał.</li></ul>">

<!ENTITY netOffline.title "Offline-modus">
<!ENTITY netOffline.longDesc2 "<ul> <li>Klikńće na &quot;Hišće raz&quot;, zo byšće do online-modusa přešoł a stronu znowa začitał.</li> </ul>">

<!ENTITY contentEncodingError.title "Zmylk při kodowanju wobsaha">
<!ENTITY contentEncodingError.longDesc "<ul> <li>Prošu stajće so z wobsedźerjemi websydła do zwiska, zo byšće jich wo tutym problemje informował.</li> </ul>">

<!ENTITY unsafeContentType.title "Njewěsty datajowy typ">
<!ENTITY unsafeContentType.longDesc "<ul> <li>Prošu stajće so z wobsedźerjemi websydła do zwiska, zo byšće jich wo tutym problemje informował.</li> </ul>">

<!ENTITY netReset.title "Zwisk bu wróćo stajeny">
<!ENTITY netReset.longDesc "&sharedLongDesc;">

<!ENTITY netTimeout.title "Zwisk je čas překročił">
<!ENTITY netTimeout.longDesc "&sharedLongDesc;">

<!ENTITY unknownProtocolFound.title "Adresa njebu zrozumjena">
<!ENTITY unknownProtocolFound.longDesc "<ul> <li>Snano dyrbiće druhe programy instalować, zo by so tuta adresa wočiniła.</li> </ul>">

<!ENTITY proxyConnectFailure.title "Proksy-serwer zwiski wotpokazuje">
<!ENTITY proxyConnectFailure.longDesc "<ul> <li>Přepruwujće proksy-nastajenja, zo by so zawěsćiło, zo wone su korektne.</li> <li>Skontaktujće swojeho syćoweho administratora, zo by so zawěsćiło, zo proksy-serwer dźěła.</li> </ul>">

<!ENTITY proxyResolveFailure.title "Njeje móžno, proksy-serwer namakać">
<!ENTITY proxyResolveFailure.longDesc "<ul> <li>Přepruwujće proksy-nastajenja, zo by so zawěsćiło, zo su korektne.</li> <li>Pruwujće, hač waš ličak ma fungowacy syćowy zwisk.</li> <li>Jeli waš ličak abo syć so z wohnjowej murju abo proksy škita, zawěsćće, zo &brandShortName; smě na Web přistup měć.</li> </ul>">

<!ENTITY redirectLoop.title "Strona njesprawje posrědkuje">
<!ENTITY redirectLoop.longDesc "<ul> <li>Tutón problem so druhdy přeze znjemóžnjenje abo wotpokazowanje plackow zawinuje.</li> </ul>">

<!ENTITY unknownSocketType.title "Njewočakowana wotmołwa ze serwera">
<!ENTITY unknownSocketType.longDesc "<ul> <li>Pruwujće, hač Personal Security Manager je instalowany na wašim systemje.</li> <li>Přičina je snano njestandardna konfiguracija na wašim serwerje.</li> </ul>">

<!ENTITY nssFailure2.title "Wěsty zwisk móžny njeje">
<!ENTITY nssFailure2.longDesc2 "<ul> <li>Strona, kotruž chceće sej wobhladać, njeda so pokazać, dokelž awtentiskosć přijatych datow njeda so přepruwować.</li> <li>Prošu stajće so z wobsedźerjemi websydła do zwiska, zo byšće jich wo tutym problemje informował.</li> </ul>">

<!ENTITY certerror.longpagetitle1 "Waš zwisk wěsty njeje">
<!ENTITY certerror.longpagetitle2 "Warnowanje: Potencielne wěstotne riziko prědku">
<!-- Localization note (certerror.introPara, certerror.introPara2) - The text content of the span tag
will be replaced at runtime with the name of the server to which the user
was trying to connect. -->
<!ENTITY certerror.introPara "Mějićel <span class='hostname'/> je swoje websydło wopak konfigurował. Zo byšće swoje informacije přećiwo tomu škitał, zo so kradnu, njeje &brandShortName; z tutym websydłom zwjazał.">

<!ENTITY certerror.introPara2 "&brandShortName; je potencielne wohroženje wěstoty a njeje tohodla zwjazał z <span class='hostname'/>. Jeli so tute sydło wopytujeće, móhli nadpadnicy spytać, daty kaž waše hesło, e-mejlowe adresy abo podrobnosće kreditnych kartow kradnyć.">

<!ENTITY certerror.expiredCert.secondPara "Tutón problem so najskerje zawinuje, dokelž časnik wašeho ličaka wopačny čas wužiwa, štož &brandShortName; při wěstym zwjazowanju haći.">

<!ENTITY certerror.whatCanYouDoAboutItTitle "Što móžeće přećiwo tomu činić?">

<!ENTITY certerror.unknownIssuer.whatCanYouDoAboutIt "<p>Najskerje websydło problem zawinuje, a njeje ničo, štož móžeće činić, zo byšće jón rozrisał.</p>
<p>Jeli słušeće k předewzaćowej syći abo antiwirusowu softwaru wužiwaće, móžeće teamy pomocy wo podpěru prosyć. Móžeće tež administratora websydła wo problemje informować.</p>">

<!ENTITY certerror.expiredCert.whatCanYouDoAboutIt2 "
<p>Waš ličakowy časnik je na <span id='wrongSystemTime_systemDate2'/> nastajeny. Kontrolujće, hač su korektny datum, korektny čas a korektne časowe pasmo w systemowych nastajenjach wašeho ličaka nastajene a aktualizujće potom <span class='hostname'/>.</p>
<p>Jeli waš časnik hižo korektny čas pokazuje, je websydło najskerje wopak konfigurowane a njemóžeće ničo činić, zo byšće tutón problem rozrisał. Móžeće administratora websydła wo tutym problemje informować.</p>">

<!ENTITY certerror.badCertDomain.whatCanYouDoAboutIt "<p>Najskerje websydło problem zawinuje, a njeje ničo, štož móžeće činić, zo byšće jón rozrisał. Móžeće administratora websydła wo problemje informować.</p>">

<!ENTITY sharedLongDesc "<ul> <li>Sydło móhło nachwilu k dispoziciji njestać abo přećežene być. Spytajcé za mało wokomikow hišće raz.</li> <li>Jeli njemóžeće někotre strony začitać, přepruwujće zwisk wašeje ličakoweje syće.</li> <li>Jeli waš ličak abo syć so z wohnjowej murju abo proksy škita, zawěsćće, zo &brandShortName; smě na Web přistup měć.</li> </ul>">

<!ENTITY cspBlocked.title "Přez wěstotne prawidła za wobsah zablokowany">
<!ENTITY cspBlocked.longDesc "<p>&brandShortName; je začitanju tuteje strony na tute wašnje zadźěwał, dokelž strona ma wěstotne prawidła za wobsah, kotrež to njedowoleja.</p>">

<!ENTITY corruptedContentErrorv2.title "Zmylk - wobškodźeny wobsah">
<!ENTITY corruptedContentErrorv2.longDesc "<p>Strona, kotruž chceće sej wobhladać, njeda so pokazać, dokelž je so zmylk při přenošowanju datow namakał.</p><ul><li>Prošu stajće so z wobsedźerjemi websydła do zwiska, zo byšće jich wo tutym problemje informował.</li></ul>">


<!ENTITY securityOverride.exceptionButtonLabel "Wuwzaće přidać…">

<!ENTITY securityOverride.exceptionButton1Label "Riziko akceptować a pokročować">

<!ENTITY errorReporting.automatic2 "Zdźělće zmylki kaž tute, zo byšće Mozilla pomhał, złowólne sydła identifikował a blokował">
<!ENTITY errorReporting.learnMore "Dalše informacije…">

<!ENTITY remoteXUL.title "Zdaleny XUL">
<!ENTITY remoteXUL.longDesc "<p><ul><li>Prošu stajće so z wobsedźerjemi websydła do zwiska, zo byšće jich wo problemje informował.</li></ul></p>">

<!ENTITY sslv3Used.title "Wěsty zwisk móžny njeje">
<!-- LOCALIZATION NOTE (sslv3Used.longDesc2) - Do not translate
     "SSL_ERROR_UNSUPPORTED_VERSION". -->
<!ENTITY sslv3Used.longDesc2 "Rozšěrjena info: SSL_ERROR_UNSUPPORTED_VERSION">

<!-- LOCALIZATION NOTE (certerror.wrongSystemTime2,
                        certerror.wrongSystemTimeWithoutReference) - The <span id='..' />
     tags will be injected with actual values, please leave them unchanged. -->
<!ENTITY certerror.wrongSystemTime2 "<p> &brandShortName; njeje z <span id='wrongSystemTime_URL'/> zwjazał, dokelž zda so, zo časnik wašeho ličaka wopačny čas pokazuje a to zadźěwa wěstemu zwiskej.</p> <p>Waš ličak je so na <span id='wrongSystemTime_systemDate'/> stajił, hdyž podaće měło <span id='wrongSystemTime_actualDate'/> być. Zo byšće tutón problem rozrisał, změńće swoje datumowe a časowe nastajenja, zo byšće korektny čas nastajił.</p>">
<!ENTITY certerror.wrongSystemTimeWithoutReference "<p>&brandShortName; njeje z <span id='wrongSystemTimeWithoutReference_URL'/> zwjazał, dokelž zda so, zo časnik wašeho ličaka wopačny čas pokazuje a to zadźěwa wěstemu zwiskej.</p> <p>Waš ličak je so na <span id='wrongSystemTimeWithoutReference_systemDate'/> stajił. Zo byšće tutón problem rozrisał, změńće swoje datumowe a časowe nastajenja, zo byšće korektny čas nastajił.</p>">

<!ENTITY certerror.pagetitle1  "Njewěsty zwisk">
<!ENTITY certerror.pagetitle2  "Warnowanje: Potencielne wěstotne riziko prědku">
<!ENTITY certerror.whatShouldIDo.badStsCertExplanation "Tute sydło wužiwa HTTP Strict Transport Security (HSTS), zo by podało, zo &brandShortName; ma jenož wěsće z nim zwjazać. Potajkim njeje móžno, wuwzaće za tutón certifikat přidać.">
<!ENTITY certerror.whatShouldIDo.badStsCertExplanation1 "<span class='hostname'></span> ma wěstotne prawidło z mjenom HTTP Strict Transport Security (HSTS), kotrež woznamjenja, zo &brandShortName; móže so jenož wěsće zwjazać. Njemóžeće wuwzaće přidać, zo byšće tute sydło wopytał.">
<!ENTITY certerror.copyToClipboard.label "Tekst do mjezyskłada kopěrować">

<!ENTITY inadequateSecurityError.title "Waš zwisk wěsty njeje">
<!-- LOCALIZATION NOTE (inadequateSecurityError.longDesc) - Do not translate
     "NS_ERROR_NET_INADEQUATE_SECURITY". -->
<!ENTITY inadequateSecurityError.longDesc "<p><span class='hostname'></span> wužiwa wěstotnu technologiju, kotraž je zestarjena a sensibelna za nadpady. Nadpadowar móhł informacije lochko wotkryć, kotrež za wěste maće. Websydłowy administrator dyrbi problem na serwerje rozrisać, prjedy hač móžeće sydło wopytać.</p><p>Zmylkowy kode: NS_ERROR_NET_INADEQUATE_SECURITY</p>">

<!ENTITY blockedByPolicy.title "Zablokowana strona">

<!ENTITY clockSkewError.title "Waš ličakowy časnik wopak dźe">
<!ENTITY clockSkewError.longDesc "Waš ličak měni, zo čas je <span id='wrongSystemTime_systemDate1'/>, štož wěstemu zwjazowanju &brandShortName; zadźěwa. Zo byšće<span class='hostname'></span> wopytał, aktualizujće časnik swojeho ličaka w systemowych nastajenjach na aktualny datum, aktualny čas a aktualne časowe pasmo a aktualizujće potom <span class='hostname'></span>.">

<!ENTITY prefReset.longDesc "Zda so, zo waše nastajenja syćoweje wěstoty móhli to zawinować. Chceće standardne nastajenja wobnowić?">
<!ENTITY prefReset.label "Standardne nastajenja wobnowić">
