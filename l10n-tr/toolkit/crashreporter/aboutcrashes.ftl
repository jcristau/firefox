# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Çökme Raporları
clear-all-reports-label = Tüm raporları sil
delete-confirm-title = Emin misiniz?
delete-confirm-description = Bu işlem tüm raporları siler ve geri alınamaz.
crashes-unsubmitted-label = Gönderilmemiş çökme raporları
id-heading = Rapor numarası
date-crashed-heading = Çökme tarihi
crashes-submitted-label = Gönderilen çökme raporları
date-submitted-heading = Gönderim tarihi
no-reports-label = Henüz hiç hata raporu gönderilmedi.
no-config-label = Bu uygulama henüz çökme raporlarını göstermek üzere ayarlanmadı. <code>breakpad.reportURL</code> tercihi ayarlanmış olmalı.
