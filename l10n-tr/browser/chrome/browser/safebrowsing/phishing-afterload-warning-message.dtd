<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Geri dön">
<!ENTITY safeb.palm.seedetails.label "Ayrıntıları göster">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Bu site aldatıcı değil…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "d">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Tavsiyeler <a id='advisory_provider'/> tarafından sağlanmaktadır.">


<!ENTITY safeb.blocked.malwarePage.title2 "Bu site bilgisayarınıza zarar verebilir">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "Bu sayfa, bilgisayarınızdaki kişisel bilgilerinizi çalabilecek veya silebilecek kötü amaçlı yazılımlar kurmaya çalışabilir. O yüzden &brandShortName; bu sayfayı engelledi.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> sitesinin <a id='error_desc_link'>kötü amaçlı yazılımlara ev sahipliği yaptığı rapor edildi</a>. Böyle olmadığını düşünüyorsanız <a id='report_detection'>bunu bildirin</a> veya <a id='ignore_warning_link'>tehlikeyi göze alarak</a> güvensiz olan bu siteyi ziyaret edin.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> sitesinin <a id='error_desc_link'>kötü amaçlı yazılımlara ev sahipliği yaptığı rapor edildi</a>. Böyle olmadığını düşünüyorsanız <a id='report_detection'>bunu bildirin</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "<a id='learn_more_link'>StopBadware.org</a> adresinden virüsler ve diğer kötü amaçlı yazılımlar gibi zararlı web içerikleri hakkında bilgi alabilir, bilgisayarınızı onlardan korumayı öğrenebilirsiniz. <a id='firefox_support'>support.mozilla.org</a> adresinden &brandShortName; yemleme ve kötü amaçlı yazılım koruması hakkında daha fazla bilgi alabilirsiniz.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Bu sitede zararlı programlar olabilir">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "Bu sayfa, gezinti deneyiminizi zarara uğratacak (örn. giriş sayfanızı değiştirerek veya ziyaret ettiğiniz sitelerde daha fazla reklam göstererek) programlar kurmak üzere sizi kandırmaya çalışabilir. O yüzden &brandShortName; bu sayfayı engelledi.">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> sitesinin <a id='error_desc_link'>zararlı yazılım içerdiği rapor edildi</a>. <a id='ignore_warning_link'>Tehlikeyi göze alıp</a> güvensiz olan bu siteyi ziyaret edebilirsiniz.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> sitesinin <a id='error_desc_link'>zararlı yazılım içerdiği rapor edildi</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "<a id='learn_more_link'>İstenmeyen Yazılım Politikası</a>’ndan zararlı ve istenmeyen yazılımlar hakkında daha fazla bilgiye ulaşabilirsiniz. <a id='firefox_support'>support.mozilla.org</a> adresinden &brandShortName; yemleme ve kötü amaçlı yazılım koruması hakkında daha fazla bilgi alabilirsiniz.">


<!ENTITY safeb.blocked.phishingPage.title3 "Aldatıcı site">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "Bu sayfa, tehlikeli bir yazılım kurmanız ya da parolalarınız veya kredi kartı bilgileriniz gibi kişisel bilgilerinizi vermeniz için sizi kandırmaya çalışabilir. O yüzden &brandShortName; bu sayfayı engelledi.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> sitesinin <a id='error_desc_link'>aldatıcı site olduğu rapor edildi</a>. Böyle olmadığını düşünüyorsanız <a id='report_detection'>bunu bildirin</a> veya <a id='ignore_warning_link'>tehlikeyi göze alarak</a> güvensiz olan bu siteyi ziyaret edin.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> sitesinin <a id='error_desc_link'>aldatıcı site olduğu rapor edildi</a>. Böyle olmadığını düşünüyorsanız <a id='report_detection'>bunu bildirin</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "<a id='learn_more_link'>www.antiphishing.org</a> sitesinden aldatıcı siteler ve yemleme hakkında daha fazla bilgiye ulaşabilirsiniz. &brandShortName; tarayıcısının yemleme ve zararlı yazılım koruması hakkında daha fazla bilgiye <a id='firefox_support'>support.mozilla.org</a> adresinden ulaşabilirsiniz.">


<!ENTITY safeb.blocked.harmfulPage.title "Bu sitede kötü amaçlı yazılımlar olabilir">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "Bu sayfa, bilgilerinizi (örn. fotoğraflar, parolalar, mesajlar ve kredi kartları) çalmayı veya silmeyi amaçlayan tehlikeli uygulamalar yüklemeye çalışabilir. O yüzden &brandShortName; bu sayfayı engelledi.">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> sitesinin <a id='error_desc_link'>potansiyel zararlı uygulama içerdiği rapor edildi</a>. <a id='ignore_warning_link'>Tehlikeyi göze alarak</a> güvensiz olan bu siteyi ziyaret edin.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> sitesinin <a id='error_desc_link'>potansiyel zararlı uygulama içerdiği rapor edildi</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "&brandShortName; tarayıcısının yemleme ve zararlı yazılım koruması hakkında daha fazla bilgiye <a id='firefox_support'>support.mozilla.org</a> adresinden ulaşabilirsiniz.">
