<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "Geliştirici Araçları Hakkında">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Firefox Geliştirici Araçları’nı etkinleştir">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "Öğeyi incele özelliğini kullanmak için Firefox Geliştirici Araçları’nı etkinleştirin">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "Geliştirici Araçları’ndaki Denetçi ile HTML ve CSS kodlarını inceleyip düzenleyin.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "Firefox Geliştirici Araçları ile WebExtension’ları, web worker’ları, service worker’ları ve daha fazlasını geliştirip hatalarını ayıklayın.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "Bir Geliştirici Araçları kısayolunu etkinleştirdiniz. Yanlışlıkla olduysa bu sekmeyi kapatabilirsiniz.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage
          "Denetçi ve Hata Ayıklayıcı gibi araçlarımızla HTML, CSS ve JavaScript kodlarını inceleyin, düzenleyin ve hatalarını ayıklayın.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "Denetçi ve Hata Ayıklayıcı gibi araçlarımızla web sitenizin HTML, CSS ve JavaScript kodlarını mükemmelleştirin.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "Tarayıcınızı daha rahat kullanabilmeniz için Firefox Geliştirici araçları varsayılan olarak kapalı gelir.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "Geliştirici Araçları hakkında daha fazla bilgi al">

<!ENTITY  aboutDevtools.enable.enableButton "Geliştiri Araçları’nı etkinleştir">
<!ENTITY  aboutDevtools.enable.closeButton "Bu sayfayı kapat">

<!ENTITY  aboutDevtools.enable.closeButton2 "Bu sekmeyi kapat">

<!ENTITY  aboutDevtools.welcome.title "Firefox Geliştirici Araçları’na hoş geldiniz!">

<!ENTITY  aboutDevtools.newsletter.title "Mozilla Geliştirici Bülteni">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "Geliştiricilere yönelik haberler, ipuçları ve kaynaklar e-posta adresinize gelsin.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "E-posta">
<!ENTITY  aboutDevtools.newsletter.privacy.label "Mozilla’nın bilgilerimi bu <a class='external' href='https://www.mozilla.org/privacy/'>gizlilik politikasında</a> açıklandığı şekilde kullanmasına onay veriyorum.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "Abone ol">
<!ENTITY  aboutDevtools.newsletter.thanks.title "Teşekkürler!">
<!ENTITY  aboutDevtools.newsletter.thanks.message "Daha önce bir Mozilla bültenine abone olmadıysanız aboneliğinizi onaylamanız gerekebilir. Lütfen gelen kutunuzu veya spam klasörünüzü kontrol edin.">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "Geliştirici Araçları’ndan daha fazlasını mı istiyorsunuz? Geliştiriciler ve çağdaş iş akışları için özel olarak hazırladığımız Firefox tarayıcısını deneyin.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "Daha fazla bilgi al">

