# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE(pageSubtitle):
# - %1$S is replaced by the value of the toolkit.telemetry.server_owner preference
# - %2$S is replaced by brandFullName
pageSubtitle = ടെലിമെട്രി ശേഖരിയ്ക്കുന്ന പ്രവര്‍ത്തനം, ഹാര്‍ഡ്‌വെയര്‍, ഉപയോഗം, യഥേഷ്ടമുള്ളവ എന്നിങ്ങനെയുള്ള വിവരങ്ങള്‍ ഈ താള്‍ കാണിയ്ക്കുന്നു. ഈ വിവരം %1$S-ലേക്കു് സമര്‍പ്പിയ്ക്കുന്നു, ഇതു് %2$S മെച്ചപ്പെടുത്തുന്നതിനായി സഹായിയ്ക്കുന്നു.

# LOCALIZATION NOTE(homeExplanation):
# - %1$S is either telemetryEnabled or telemetryDisabled
# - %2$S is either extendedTelemetryEnabled or extendedTelemetryDisabled
telemetryEnabled = സജീവം
telemetryDisabled = നിർജീവം
extendedTelemetryEnabled = സജീവം
extendedTelemetryDisabled = നിർജീവം

# LOCALIZATION NOTE(settingsExplanation):
# - %1$S is either releaseData or prereleaseData
# - %2$S is either telemetryUploadEnabled or telemetryUploadDisabled
releaseData = റിലീസ് ഡാറ്റ
prereleaseData = പ്രീ-റിലീസ് ഡാറ്റ
telemetryUploadEnabled = സജീവം
telemetryUploadDisabled = നിർജീവം

# LOCALIZATION NOTE(pingDetails):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by namedPing
# LOCALIZATION NOTE(namedPing):
# - %1$S is replaced by the ping localized timestamp, e.g. “2017/07/08 10:40:46”
# - %2$S is replaced by the ping name, e.g. “saved-session”
namedPing = %1$S, %2$S
# LOCALIZATION NOTE(pingDetailsCurrent):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by currentPing
currentPing = നിലവിലുള്ളത്

# LOCALIZATION NOTE(filterPlaceholder): string used as a placeholder for the
# search field, %1$S is replaced by the section name from the structure of the
# ping. More info about it can be found here:
# https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
filterPlaceholder = %1$S ൽ കണ്ടെത്തുക
filterAllPlaceholder = എല്ലാ വിഭാഗങ്ങളിലും കണ്ടെത്തുക

# LOCALIZATION NOTE(resultsForSearch): %1$S is replaced by the searched terms
resultsForSearch = “%1$S” എന്നതിനായുള്ള ഫലങ്ങൾ
# LOCALIZATION NOTE(noSearchResults):
# - %1$S is replaced by the section name from the structure of the ping.
# More info about it can be found here: https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
# - %2$S is replaced by the current text in the search input
noSearchResults = ക്ഷമിക്കണം! “%2$S” എന്നതിനായി %1$S ൽ ഫലങ്ങൾ ഒന്നുമില്ല
# LOCALIZATION NOTE(noSearchResultsAll): %S is replaced by the searched terms
noSearchResultsAll = ക്ഷമിക്കണം! “%S” നു വേണ്ടി യാതൊരു വിഭാഗങ്ങളിലും ഫലങ്ങളൊന്നുമില്ല
# LOCALIZATION NOTE(noDataToDisplay): %S is replaced by the section name.
# This message is displayed when a section is empty.
noDataToDisplay = ക്ഷമിക്കണം! “%S” ൽ നിലവിൽ ഡാറ്റയൊന്നും ലഭ്യമല്ല
# LOCALIZATION NOTE(currentPingSidebar): used as a tooltip for the “current”
# ping title in the sidebar
# LOCALIZATION NOTE(telemetryPingTypeAll): used in the “Ping Type” select
telemetryPingTypeAll = എല്ലാം

# LOCALIZATION NOTE(histogram*): these strings are used in the “Histograms” section
histogramSamples = മാതൃകകള്‍
histogramAverage = ശരാശരി
histogramSum = മൊത്തം
# LOCALIZATION NOTE(histogramCopy): button label to copy the histogram
histogramCopy = പകര്‍ത്തുക

# LOCALIZATION NOTE(telemetryLog*): these strings are used in the “Telemetry Log” section
telemetryLogTitle = ടെലിമെട്രി ലോഗ്
telemetryLogHeadingId = ഐഡി
telemetryLogHeadingTimestamp = സമയമുദ്ര
telemetryLogHeadingData = ഡേറ്റാ

# LOCALIZATION NOTE(slowSql*): these strings are used in the “Slow SQL Statements” section
slowSqlMain = പ്രധാന ത്രെഡില്‍ എസ്‌ക്യൂഎല്‍ സ്റ്റേറ്റ്മെന്റുകളുടെ വേഗത കുറയ്ക്കുക
slowSqlOther = സഹായത്തിനുള്ള ത്രെഡില്‍ എസ്‌ക്യൂഎല്‍ സ്റ്റേറ്റ്മെന്റുകളുടെ വേഗത കുറയ്ക്കുക
slowSqlHits = ഹിറ്റുകള്‍
slowSqlAverage = ശരാശരി സമയം (ms)
slowSqlStatement = വാചകം

# LOCALIZATION NOTE(histogram*): these strings are used in the “Add-on Details” section
addonTableID = ആഡ്-ഓണ്‍ ഐഡി
addonTableDetails = വിശദാംശങ്ങള്‍
# LOCALIZATION NOTE(addonProvider):
# - %1$S is replaced by the name of an Add-on Provider (e.g. “XPI”, “Plugin”)
addonProvider = %1$S പ്രൊവൈഡര്‍

keysHeader = വിശേഷത
namesHeader = പേര്
valuesHeader = മൂല്യം

# LOCALIZATION NOTE(chrome-hangs-title):
# - %1$S is replaced by the number of the hang
# - %2$S is replaced by the duration of the hang
chrome-hangs-title = ഹാങ് റിപോര്‍ട്ട്#%1$S (%2$S സെക്കന്‍ഡുകള്‍)
# LOCALIZATION NOTE(captured-stacks-title):
# - %1$S is replaced by the string key for this stack
# - %2$S is replaced by the number of times this stack was captured
# LOCALIZATION NOTE(late-writes-title):
# - %1$S is replaced by the number of the late write
late-writes-title = Late Write #%1$S

stackTitle = സ്റ്റാക്ക്:
memoryMapTitle = മെമ്മറി മാപ്പ്:

errorFetchingSymbols = ചിഹ്നങ്ങള്‍ ലഭ്യമാക്കുമ്പോള്‍ ഒരു പിശകുണ്ടായിരിയ്ക്കുന്നു. നിങ്ങള്‍ക്കു് ഇന്റര്‍നെറ്റ് കണക്ഷനുണ്ടെന്നുറപ്പാക്കി വീണ്ടും ശ്രമിയ്ക്കുക.

# LOCALIZATION NOTE(childPayloadN):
# - %1$S is replaced by the number of the child payload (e.g. “1”, “2”)
