# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = തടയല്‍ പട്ടികകൾ
    .style = width: 55em
blocklist-desc = ഫയര്‍ഫോക്സ് ഏത് പട്ടിക താങ്കളുടെ ബ്രൗസിംഗ് കർമങ്ങളെ നിരീക്ഷിക്കുന്ന വെബ് എലമെന്റുകൾ തടയാനായി  ഉപയോഗിക്കുമെന്ന് താങ്കൾക്ക് തീരുമാനിക്കാം.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = പട്ടിക
blocklist-button-cancel =
    .label = റദ്ദാക്കുക
    .accesskey = C
blocklist-button-ok =
    .label = മാറ്റങ്ങള്‍ സൂക്ഷിക്കുക
    .accesskey = S
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Disconnect.me അടിസ്ഥാന സംരക്ഷണം (ശുപാര്‍ശ ചെയ്യുന്നു)
blocklist-item-moz-std-desc = വെബ്‍സൈറ്റുകള്‍ നന്നായി പ്രവര്‍ത്തിക്കാന്‍ ചില ട്രാക്കറുകളെ അനുവദിക്കുക.
blocklist-item-moz-full-name = Disconnect.me കർശന സംരക്ഷണം.
blocklist-item-moz-full-desc = അറിയാവുന്ന ട്രാക്കറുകളെ തടയുന്നു. ചില വെബ്സൈറ്റുകൾ ശരിയായി പ്രവർത്തിച്ചേക്കില്ല.
