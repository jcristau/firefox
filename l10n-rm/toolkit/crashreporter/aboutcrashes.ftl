# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Annunzias da collaps
clear-all-reports-label = Allontanar tut las annunzias
delete-confirm-title = Allontanar las annunzias
delete-confirm-description = Quai stizza tut las annunzias e na po betg pli vegnir revocà.
crashes-unsubmitted-label = Annunzias da collaps betg tramessas
id-heading = ID da l'annunzia
date-crashed-heading = Data dal collaps
crashes-submitted-label = Annunzias da collaps tramessas
date-submitted-heading = Data da transmissiun
no-reports-label = Fin ussa n'èn vegnidas tramessas anc naginas annunzias da collaps.
no-config-label = Questa applicaziun n'è betg vegnida configurada per mussar annunzias da collaps. La preferenza <code>breakpad.reportURL</code> sto vegnir exequida.
