# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Linguas
    .style = width: 37em
languages-close-key =
    .key = w
languages-description = Paginas d'internet èn mintgatant disponiblas en pliras linguas. Tscherna las linguas per mussar questas paginas, en la successiun da tia preferenza
languages-customize-spoof-english =
    .label = Dumandar versiuns englaisas da paginas d'internet per augmentar la protecziun da datas
languages-customize-moveup =
    .label = Ensi
    .accesskey = E
languages-customize-movedown =
    .label = Engiu
    .accesskey = u
languages-customize-remove =
    .label = Allontanar
    .accesskey = l
languages-customize-select-language =
    .placeholder = Tscherna ina lingua per agiuntar…
languages-customize-add =
    .label = Agiuntar
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale } [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
