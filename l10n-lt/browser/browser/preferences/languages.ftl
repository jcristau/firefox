# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Kalbos
    .style = width: 32em
languages-close-key =
    .key = w
languages-description = Tinklalapiai kartais yra prienami daugiau nei viena kalba. Pasirinkite kalbas šių tinklalapių atvaizdavimui, išdėstydami pagal pirmenybę
languages-customize-spoof-english =
    .label = Užklausti tinklalapių versijų anglų kalba dėl didesnio privatumo
languages-customize-moveup =
    .label = Pakelti
    .accesskey = P
languages-customize-movedown =
    .label = Nuleisti
    .accesskey = N
languages-customize-remove =
    .label = Pašalinti
    .accesskey = š
languages-customize-select-language =
    .placeholder = Parinkite kalbą…
languages-customize-add =
    .label = Įtraukti
    .accesskey = r
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
