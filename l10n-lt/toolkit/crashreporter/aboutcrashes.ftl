# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Strigčių pranešimai
clear-all-reports-label = Pašalinti visus pranešimus
delete-confirm-title = Ar tikrai to norite?
delete-confirm-description = Visi pranešimai bus negrįžtamai pašalinti.
crashes-unsubmitted-label = Neišsiųsti strigčių pranešimai
id-heading = Pranešimo ID
date-crashed-heading = Strigties data
crashes-submitted-label = Išsiųsti strigčių pranešimai
date-submitted-heading = Pranešimo data
no-reports-label = Išsiųstų strigčių pranešimų nėra.
no-config-label = Ši programa nėra suderinta, siųsti strigčių pranešimus. Tam reikia, kad būtų nurodyta nuostatos <code>breakpad.reportURL</code> reikšmė.
