# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = ავარიული დახურვის მოხსენებები
clear-all-reports-label = ყველა მოხსენების წაშლა
delete-confirm-title = დარწმუნებული ხართ?
delete-confirm-description = შედეგად, ყველა მოხსენება წაიშლება და ვეღარ აღდგება.
crashes-unsubmitted-label = გადაუგზავნელი მოხსენებები
id-heading = ანგარიშგების ID
date-crashed-heading = ავარიული დახურვის თარიღი
crashes-submitted-label = გადაგზავნილი მოხსენებები
date-submitted-heading = გაგზავნის თარიღი
no-reports-label = ავარიული დახურვის მოხსენებები არ არის.
no-config-label = ეს პროგრამა ავარიული დახურვის მოხსენებების საჩვენებლად არაა გამართული. უნდა მიეთითოს პარამეტრი <code>breakpad.reportURL</code>.
