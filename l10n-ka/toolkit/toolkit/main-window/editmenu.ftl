### This file contains the entities needed for the 'edit' menu
### It's currently only used for the Browser Console

editmenu-undo =
    .label = დაბრუნება
    .accesskey = დ
editmenu-redo =
    .label = კვლავ შესრულება
    .accesskey = კ
editmenu-cut =
    .label = ამოჭრა
    .accesskey = ჭ
editmenu-copy =
    .label = დაკოპირება
    .accesskey = პ
editmenu-paste =
    .label = ჩასმა
    .accesskey = ჩ
editmenu-delete =
    .label = წაშლა
    .accesskey = წ
editmenu-select-all =
    .label = ყველას მონიშვნა
    .accesskey = ყ
