# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = ენები
    .style = width: 32em
languages-close-key =
    .key = w
languages-description = ვებგვერდები ხანდახან ერთზე მეტ ენაზეა შემოთავაზებული. აირჩიეთ ასეთი გვერდებისთვის სასურველი ენები უპირატესობის მიხედვით
languages-customize-spoof-english =
    .label = ვებგვერდების ინგლისურ ენაზე მოთხოვნა, მეტი პირადულობისთვის
languages-customize-moveup =
    .label = აწევა
    .accesskey = წ
languages-customize-movedown =
    .label = ჩამოწევა
    .accesskey = ჩ
languages-customize-remove =
    .label = მოცილება
    .accesskey = მ
languages-customize-select-language =
    .placeholder = დასამატებელი ენის შერჩევა…
languages-customize-add =
    .label = დამატება
    .accesskey = მ
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
