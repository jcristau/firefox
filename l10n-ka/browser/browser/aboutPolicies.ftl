# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = დებულებები კომპანიებისთვის
# 'Active' is used to describe the policies that are currently active
active-policies-tab = მოქმედი
errors-tab = შეცდომები
documentation-tab = მასალები
policy-name = დებულების სახელი
policy-value = დებულების მნიშვნელობა
policy-errors = დებულების შეცდომები
