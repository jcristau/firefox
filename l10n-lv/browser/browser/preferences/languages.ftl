# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Valodas
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Tīmekļa lappuses dažreiz tiek piedāvātas vairākās valodās. Izvēlēties šo tīmekļa lappušu rādīšanas valodu izvēles kārtību
languages-customize-spoof-english =
    .label = Pieprasīt lapu angļu valodas versiju, lai uzlabotu privātumu
languages-customize-moveup =
    .label = Pārvietot augšup
    .accesskey = a
languages-customize-movedown =
    .label = Pārvietot lejup
    .accesskey = l
languages-customize-remove =
    .label = Aizvākt
    .accesskey = A
languages-customize-select-language =
    .placeholder = Pievienot valodu...
languages-customize-add =
    .label = Pievienot
    .accesskey = P
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
