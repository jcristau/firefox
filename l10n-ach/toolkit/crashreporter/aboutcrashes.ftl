# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Ripot me poto
clear-all-reports-label = Kwany Ngec Woko Weng
delete-confirm-title = Imoko ada?
delete-confirm-description = Man bikwanyo ripot weng kadong gonyo ne pe lare.
crashes-unsubmitted-label = Ngec me poto ma pe kicwalo
id-heading = Cwal ngec me ID
date-crashed-heading = Nino dwe opoto woko
crashes-submitted-label = Omiyo ngec me poto
date-submitted-heading = Kicwalo Nino dwe
no-reports-label = Pe kicwalo ngec me opoto.
no-config-label = Purugram man pe kicano me yaro ka maleng ngec me poto. En ma imito <code>breakpad.reportURL</code> myero kiter.
