# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Dillər
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Bəzi səhifələr birdən çox dil dəstəyi verə bilər. Göstəriləcək dil sıralamasını aşağıdakı pəncərədən seçə bilərsiniz
languages-customize-spoof-english =
    .label = Artırılmış məxfilik üçün saytların İngiliscə versiyalarını istə
languages-customize-moveup =
    .label = Yuxarıya
    .accesskey = u
languages-customize-movedown =
    .label = Aşağı apar
    .accesskey = A
languages-customize-remove =
    .label = Sil
    .accesskey = S
languages-customize-select-language =
    .placeholder = Əlavə ediləcək dili seçin…
languages-customize-add =
    .label = Əlavə et
    .accesskey = a
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale } [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
