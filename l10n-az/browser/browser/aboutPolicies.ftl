# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Sahibkarlıq Siyasəti
# 'Active' is used to describe the policies that are currently active
active-policies-tab = Aktiv
errors-tab = Xətalar
documentation-tab = Sənədlər
policy-name = Siyasət Adı
policy-value = Siyasət Dəyəri
policy-errors = Siyasət Xətaları
