# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Çökmə Hesabatları
clear-all-reports-label = Bütün hesabatları sil
delete-confirm-title = Emin misiniz?
delete-confirm-description = Bu əməliyyat bütün hesabatları siləcək və geri qaytarmaq mümkün olmayacaq
crashes-unsubmitted-label = Göndərilməmiş Çökmə Hesabatları
id-heading = Hesabat kimliyi
date-crashed-heading = Çökmə Tarixi
crashes-submitted-label = Göndərilmiş Çökmə Hesabatları
date-submitted-heading = Göndərilmə Tarixi
no-reports-label = Hələki heç bir xəta hesabatı göndərilməyib.
no-config-label = Bu proqram hələki çökmə hesabatı göstərməkçün nizamlanmayıb.<code>breakpad.reporturl</code> seçimi seçilmiş olmalıdır{ " " }
