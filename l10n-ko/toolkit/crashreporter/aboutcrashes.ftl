# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = 충돌 보고서
clear-all-reports-label = 보고서 모두 삭제
delete-confirm-title = 계속 하시겠습니까?
delete-confirm-description = 모든 보고서를 삭제하고 복구할 수 없습니다.
crashes-unsubmitted-label = 보내지 않은 충돌 보고서
id-heading = 보고서 ID
date-crashed-heading = 데이타 충돌됨
crashes-submitted-label = 보낸 충돌 보고서
date-submitted-heading = 데이터 보내짐
no-reports-label = 전송한 충돌 보고서가 없습니다.
no-config-label = 이 프로그램은 충돌 보고서 표시 기능이 설정되어 있지 않습니다. <code>breakpad.reportURL</code> 값을 설정해 주십시오.
