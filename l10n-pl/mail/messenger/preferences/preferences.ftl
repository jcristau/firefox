# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = Wybierz język używany do wyświetlania menu, komunikatów i powiadomień programu { -brand-short-name }.
confirm-messenger-language-change-description = Uruchom ponownie, aby zastosować te zmiany
confirm-messenger-language-change-button = Zastosuj i uruchom ponownie
