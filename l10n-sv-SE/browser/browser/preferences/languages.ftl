# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Språk
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Webbsidor erbjuds ibland på mer än ett språk. Välj vilka språk som ska visas och i vilken ordning du föredrar dem
languages-customize-spoof-english =
    .label = Begär engelska versioner av webbsidor för ökad integritet
languages-customize-moveup =
    .label = Flytta upp
    .accesskey = u
languages-customize-movedown =
    .label = Flytta ner
    .accesskey = n
languages-customize-remove =
    .label = Ta bort
    .accesskey = T
languages-customize-select-language =
    .placeholder = Välj ett språk att lägga till…
languages-customize-add =
    .label = Lägg till
    .accesskey = L
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
