# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Kraschrapporter
clear-all-reports-label = Ta bort alla rapporter
delete-confirm-title = Är du säker?
delete-confirm-description = Det här tar bort alla rapporter och kan inte ångras.
crashes-unsubmitted-label = Ej Inskickade kraschrapporter
id-heading = Rapport-ID
date-crashed-heading = Datum för krasch
crashes-submitted-label = Inskickade kraschrapporter
date-submitted-heading = Datum
no-reports-label = Inga kraschrapporter har skickats in.
no-config-label = Det här programmet är inte konfigurerat att visa kraschrapporter. Inställningen <code>breakpad.reportURL</code> måste anges.
