# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Talen
    .style = width: 34em
languages-close-key =
    .key = w
languages-description = Websiden binne somtiden beskikber yn mear as ien taal. Kies de talen foar it toanen fan dizze websiden, yn folchoarder fan foarkar
languages-customize-spoof-english =
    .label = Ingelsktalige ferzjes fan websiden opfreegje foar ferbettere privacy
languages-customize-moveup =
    .label = Omheech ferpleatse
    .accesskey = h
languages-customize-movedown =
    .label = Omleech ferpleatse
    .accesskey = l
languages-customize-remove =
    .label = Fuortsmite
    .accesskey = F
languages-customize-select-language =
    .placeholder = Selektearje in taal om ta te foegjen…
languages-customize-add =
    .label = Tafoegje
    .accesskey = T
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
