# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Ungelokrapporten
clear-all-reports-label = Alle rapporten fuortsmite
delete-confirm-title = Binne jo wis?
delete-confirm-description = Dit sil alle rapporten fuortsmite en kin net ûngedien makke wurde
crashes-unsubmitted-label = Net ynstjoerde ûngelokrapporten
id-heading = Rapport ID
date-crashed-heading = Ungelokdatum
crashes-submitted-label = Ynstjoerde ûngelokrapporten
date-submitted-heading = Ynstjoerdatum
no-reports-label = Der binne gjin ûngelokrapporten ynstjoerd.
no-config-label = Dizze applikaasje is net ynsteld om ûngelokrapporten te toanen. De foarkar <code>breakpad.reportURL</code> moat ynsteld wêze.
