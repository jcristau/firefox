# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Enpresa-politikak
# 'Active' is used to describe the policies that are currently active
active-policies-tab = Aktibo
errors-tab = Erroreak
documentation-tab = Dokumentazioa
policy-name = Politikaren izena
policy-value = Politikaren balioa
policy-errors = Politika-erroreak
