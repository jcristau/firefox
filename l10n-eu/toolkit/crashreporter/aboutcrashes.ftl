# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Hutsegite-txostenak
clear-all-reports-label = Ezabatu txosten guztiak
delete-confirm-title = Ziur zaude?
delete-confirm-description = Txosten guztiak ezabatuko dira eta ezin da desegin
crashes-unsubmitted-label = Bidali gabeko hutsegite-txostenak
id-heading = Txostenaren IDa
date-crashed-heading = Hutsegite-data
crashes-submitted-label = Bidalitako hutsegite-txostenak
date-submitted-heading = Noiz bidalia
no-reports-label = Ez da bidali hutsegite-txostenik.
no-config-label = Aplikazio hau ez da konfiguratu hutsegite-txostenak bistaratzeko. Horretarako <code>breakpad.reportURL</code> hobespena ezarri behar da.
