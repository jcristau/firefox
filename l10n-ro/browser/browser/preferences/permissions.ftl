# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = Excepții
    .style = width: 50em
permissions-close-key =
    .key = w
permissions-address = Adresa site-ului web
    .accesskey = d
permissions-block =
    .label = Blochează
    .accesskey = B
permissions-session =
    .label = Permite pentru sesiune
    .accesskey = s
permissions-allow =
    .label = Permite
    .accesskey = A
permissions-site-name =
    .label = Site web
permissions-status =
    .label = Stare
permissions-remove =
    .label = Elimină site-ul web
    .accesskey = R
permissions-remove-all =
    .label = Elimină toate site-urile web
    .accesskey = E
permissions-button-cancel =
    .label = Renunță
    .accesskey = C
permissions-button-ok =
    .label = Salvează schimbările
    .accesskey = S
permissions-searchbox =
    .placeholder = Caută site web
permissions-capabilities-allow =
    .label = Permite
permissions-capabilities-block =
    .label = Blochează
permissions-capabilities-prompt =
    .label = Întreabă întotdeauna

## Invalid Hostname Dialog

permissions-invalid-uri-title = Nume de gazdă nevalid
permissions-invalid-uri-label = Te rugăm să introduci un nume de gazdă valid

## Exceptions - Tracking Protection

permissions-exceptions-tracking-protection-window =
    .title = Excepții - Protecție față de urmărire
    .style = { permissions-window.style }
permissions-exceptions-tracking-protection-desc = Ai dezactivat protecția față de urmărire pe aceste site-uri web.

## Exceptions - Cookies

permissions-exceptions-cookie-window =
    .title = Excepții - Cookie-uri și date ale site-urilor
    .style = { permissions-window.style }
permissions-exceptions-cookie-desc = Poți specifica ce site-uri web au întotdeauna sau niciodată permisiunea de a folosi cookie-uri și date ale site-urilor.  Tastează adresa exactă a site-ului pe care vrei să-l gestionezi și apoi clic pe Blochează, Permite pentru sesiune sau Permite.

## Exceptions - Pop-ups

permissions-exceptions-popup-window =
    .title = Site-uri web permise - Ferestre pop-up
    .style = { permissions-window.style }
permissions-exceptions-popup-desc = Poți specifica ce site-uri web au permisiunea de a deschide ferestre pop-up. Tastează adresa exactă a site-ului căruia vrei să-i oferi permisiunea și apoi clic pe Permite.

## Exceptions - Saved Logins

permissions-exceptions-saved-logins-window =
    .title = Excepții - Autentificări salvate
    .style = { permissions-window.style }
permissions-exceptions-saved-logins-desc = Autentificările pentru următoarele site-uri web nu vor fi salvate

## Exceptions - Add-ons

permissions-exceptions-addons-window =
    .title = Site-uri web permise - Instalare de extensii
    .style = { permissions-window.style }
permissions-exceptions-addons-desc = Poți specifica ce site-uri web au permisiunea de a instala suplimente. Tastează adresa exactă a site-ului căruia vrei să-i oferi permisiunea și apoi clic pe Permite.

## Exceptions - Autoplay Media

permissions-exceptions-autoplay-media-window =
    .title = Site-uri web permise - Redare automată
    .style = { permissions-window.style }
permissions-exceptions-autoplay-media-desc = Poți specifica site-urile cărora le permiți să redea automat elemente multimedia. Scrie adresa exactă a site-ului și clic pe butonul Permite.
permissions-exceptions-autoplay-media-desc2 = Poți specifica ce site-uri web au întotdeauna sau niciodată permisiunea de a reda automat conținut media cu sunet. Tastează adresa a site-ului pe care vrei să-l gestionezi și apoi clic pe Blochează sau Permite.

## Site Permissions - Notifications

permissions-site-notification-window =
    .title = Setări - Permisiuni pentru notificări
    .style = { permissions-window.style }
permissions-site-notification-desc = Următoarele site-uri web au solicitat să îți trimită notificări. Poți alege ce site-uri web au permisiunea de a-ți trimite notificări. De asemenea, poți bloca noile solicitări de permitere a notificărilor.
permissions-site-notification-disable-label =
    .label = Blochează noile solicitări de permitere a notificărilor
permissions-site-notification-disable-desc = Acest lucru va împiedica orice site web care nu este listat mai sus să ceară permisiunea de a trimite notificări. Blocarea notificărilor ar putea interfera cu unele funcții ale site-urilor web.

## Site Permissions - Location

permissions-site-location-window =
    .title = Setări - Permisiuni pentru locație
    .style = { permissions-window.style }
permissions-site-location-desc = Următoarele site-uri web au solicitat să îți acceseze locația. Poți specifica ce site-uri web au permisiunea de a-ți accesa locația. De asemenea, poți bloca noile solicitări de accesare a locației tale.
permissions-site-location-disable-label =
    .label = Blochează noile solicitări de accesare a locației tale
permissions-site-location-disable-desc = Acest lucru va împiedica orice site web care nu este listat mai sus să ceară permisiunea de a-ți accesa locația. Blocarea accesului la locație ar putea interfera cu unele funcții ale site-urilor web.

## Site Permissions - Camera

permissions-site-camera-window =
    .title = Setări - Permisiuni pentru cameră
    .style = { permissions-window.style }
permissions-site-camera-desc = Următoarele site-uri web au solicitat să îți acceseze camera. Poți specifica ce site-uri web au permisiunea de a-ți accesa camera. De asemenea, poți bloca noile solicitări de accesare a camerei tale.
permissions-site-camera-disable-label =
    .label = Blochează noile solicitări de accesare a camerei tale
permissions-site-camera-disable-desc = Acest lucru va împiedica orice site web care nu este listat mai sus să ceară permisiunea de a-ți accesa camera. Blocarea accesului la cameră ar putea interfera cu unele funcții ale site-urilor web.

## Site Permissions - Microphone

permissions-site-microphone-window =
    .title = Setări - Permisiuni pentru microfon
    .style = { permissions-window.style }
permissions-site-microphone-desc = Următoarele site-uri web au solicitat să îți acceseze microfonul. Poți specifica ce site-uri web au permisiunea de a-ți accesa microfonul. De asemenea, poți bloca noile solicitări de accesare a microfonului tău.
permissions-site-microphone-disable-label =
    .label = Blochează noile solicitări de accesare a microfonului tău
permissions-site-microphone-disable-desc = Acest lucru va împiedica orice site web care nu este listat mai sus să ceară permisiunea de a-ți accesa microfonul. Blocarea accesului la microfon ar putea interfera cu unele funcții ale site-urilor web.
