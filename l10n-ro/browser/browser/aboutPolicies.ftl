# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Politici dedicate întreprinderilor
# 'Active' is used to describe the policies that are currently active
active-policies-tab = Active
errors-tab = Erori
documentation-tab = Documentație
policy-name = Numele politicii
policy-value = Valoarea politicii
policy-errors = Erori privind politicile
