# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Rapoarte de defecțiuni
clear-all-reports-label = Elimină toate rapoartele
delete-confirm-title = Sigur vrei asta?
delete-confirm-description = Acest lucru va șterge toate rapoartele și nu poate fi anulat.
crashes-unsubmitted-label = Rapoarte de defecțiuni netrimise
id-heading = ID-ul raportului
date-crashed-heading = Data defecțiunii
crashes-submitted-label = Rapoarte de defecțiuni trimise
date-submitted-heading = Data trimiterii
no-reports-label = Nu s-au trimis rapoarte de defecțiuni.
no-config-label = Această aplicație nu a fost configurată să afișeze rapoartele de defecțiuni. Preferința <code>breakpad.reportURL</code> trebuie setată.
