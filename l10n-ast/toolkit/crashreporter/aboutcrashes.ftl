# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Informes de casques
clear-all-reports-label = Desaniciar tolos informes
delete-confirm-title = ¿Tas seguru?
delete-confirm-description = Esto va desaniciar tolos informes y nun va poder desfacese.
crashes-unsubmitted-label = Informes de casques ensin unviar
id-heading = ID d'informe
date-crashed-heading = Data de casque
crashes-submitted-label = Informes de casques unviaos
date-submitted-heading = Data d'unviu
no-reports-label = Nun s'unviaron informes de casques.
no-config-label = Esta aplicación nun se configuró p'amosar informes de casques. Ha afitase la preferencia <code>breakpad.reportURL</code>.
