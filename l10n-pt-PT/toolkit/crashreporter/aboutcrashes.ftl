# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Relatórios de falha
clear-all-reports-label = Remover todos os relatórios
delete-confirm-title = Tem a certeza?
delete-confirm-description = Isto irá apagar todos os relatórios e não poderá ser desfeito.
crashes-unsubmitted-label = Relatórios de falha não submetidos
id-heading = ID do relatório
date-crashed-heading = Data da falha
crashes-submitted-label = Relatórios de falha submetidos
date-submitted-heading = Data de submissão
no-reports-label = Não foram submetidos quaisquer relatórios de falha.
no-config-label = Esta aplicação não foi configurada para mostrar relatórios de falha. A preferência <code>breakpad.reportURL</code> tem de ser definida.
