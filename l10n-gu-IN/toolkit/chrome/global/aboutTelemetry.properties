# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE(pageSubtitle):
# - %1$S is replaced by the value of the toolkit.telemetry.server_owner preference
# - %2$S is replaced by brandFullName
pageSubtitle = This page shows the information about performance, hardware, usage and customizations collected by Telemetry. This information is submitted to %1$S to help improve %2$S.

# LOCALIZATION NOTE(settingsExplanation):
# - %1$S is either releaseData or prereleaseData
# - %2$S is either telemetryUploadEnabled or telemetryUploadDisabled
settingsExplanation = ટેલીમેટ્રી %1$S એકત્રિત કરી રહ્યું છે અને અપલોડ %2$S છે.
releaseData = રિલીઝ ડેટા
prereleaseData = પ્રી-રિલીઝ ડેટા
telemetryUploadEnabled = સક્ષમ
telemetryUploadDisabled = અક્ષમ કરેલ

# LOCALIZATION NOTE(pingDetails):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by namedPing
pingDetails = દરેક ભાગની માહિતીને “%1$S” માં મોકલવામાં આવે છે. તમે %2$S પિંગ જોઈ રહ્યાં છો.
# LOCALIZATION NOTE(namedPing):
# - %1$S is replaced by the ping localized timestamp, e.g. “2017/07/08 10:40:46”
# - %2$S is replaced by the ping name, e.g. “saved-session”
namedPing = %1$S, %2$S
# LOCALIZATION NOTE(pingDetailsCurrent):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by currentPing
pingDetailsCurrent = દરેક ભાગની માહિતીને “%1$S“ માં મોકલવામાં આવે છે. તમે %2$S પિંગ જોઈ રહ્યાં છો.
pingExplanationLink = પિંગ્સ
currentPing = વર્તમાન

# LOCALIZATION NOTE(filterPlaceholder): string used as a placeholder for the
# search field, %1$S is replaced by the section name from the structure of the
# ping. More info about it can be found here:
# https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
filterPlaceholder = %1$S માં શોધો
filterAllPlaceholder = બધા વિભાગોમાં શોધો

# LOCALIZATION NOTE(resultsForSearch): %1$S is replaced by the searched terms
resultsForSearch = “%1$S” માટે પરિણામો
# LOCALIZATION NOTE(noSearchResults):
# - %1$S is replaced by the section name from the structure of the ping.
# More info about it can be found here: https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
# - %2$S is replaced by the current text in the search input
noSearchResults = માફ કરશો! “%2$S” માટે %1$S માં કોઈ પરિણામો નથી
# LOCALIZATION NOTE(noSearchResultsAll): %S is replaced by the searched terms
noSearchResultsAll = માફ કરશો! “%S” માટે કોઈ પણ વિભાગોમાં કોઈ પરિણામો નથી
# LOCALIZATION NOTE(noDataToDisplay): %S is replaced by the section name.
# This message is displayed when a section is empty.
noDataToDisplay = માફ કરશો! હાલમાં “%S” માં કોઈ ડેટા ઉપલબ્ધ નથી
# LOCALIZATION NOTE(currentPingSidebar): used as a tooltip for the “current”
# ping title in the sidebar
currentPingSidebar = વર્તમાન પિંગ
# LOCALIZATION NOTE(telemetryPingTypeAll): used in the “Ping Type” select
telemetryPingTypeAll = બધા

# LOCALIZATION NOTE(histogram*): these strings are used in the “Histograms” section
histogramSamples = નમૂના
histogramAverage = સરેરાશ
histogramSum = સરવાળો
# LOCALIZATION NOTE(histogramCopy): button label to copy the histogram
histogramCopy = નકલ

# LOCALIZATION NOTE(telemetryLog*): these strings are used in the “Telemetry Log” section
telemetryLogTitle = ટેલીમેટ્રી લોગ
telemetryLogHeadingId = Id
telemetryLogHeadingTimestamp = ટાઇમસ્ટેમ્પ
telemetryLogHeadingData = ડેટા

# LOCALIZATION NOTE(slowSql*): these strings are used in the “Slow SQL Statements” section
slowSqlMain = મુખ્ય થ્રેડ પર ધીમા SQL વાક્યો
slowSqlOther = મદદકર્તા થ્રેડ પર ધીમા SQL વાક્યો
slowSqlHits = હિટ્સ
slowSqlAverage = સરેરાશ સમય (ms)
slowSqlStatement = વાક્ય

# LOCALIZATION NOTE(histogram*): these strings are used in the “Add-on Details” section
addonTableID = ઍડ-ઑન ID
addonTableDetails = વિગતો
# LOCALIZATION NOTE(addonProvider):
# - %1$S is replaced by the name of an Add-on Provider (e.g. “XPI”, “Plugin”)
addonProvider = %1$S પ્રોવાઇડર

keysHeader = ગુણધર્મ
namesHeader = નામ
valuesHeader = કિંમત

# LOCALIZATION NOTE(chrome-hangs-title):
# - %1$S is replaced by the number of the hang
# - %2$S is replaced by the duration of the hang
chrome-hangs-title = અટકેલ અહેવાલ #%1$S (%2$S સેકંડ)
# LOCALIZATION NOTE(captured-stacks-title):
# - %1$S is replaced by the string key for this stack
# - %2$S is replaced by the number of times this stack was captured
captured-stacks-title = %1$S (કેપ્ચર ગણના: %2$S)
# LOCALIZATION NOTE(late-writes-title):
# - %1$S is replaced by the number of the late write
late-writes-title = #%1$S ને અંતમાં લખે છે

stackTitle = સ્ટેક:
memoryMapTitle = મેમરી નક્ષો:

errorFetchingSymbols = ભૂલ ઉદ્ભવી જ્યારે સંકેતને લાવી રહ્યા હોય. ચકાસો કે તમે ઇન્ટરનેટમાં જોડાયેલ છે અને ફરી પ્રયત્ન કરો.

parentPayload = પેરૈંટ પેલોડ
# LOCALIZATION NOTE(childPayloadN):
# - %1$S is replaced by the number of the child payload (e.g. “1”, “2”)
childPayloadN = ચાઇલ્ડ પેલોડ %1$S
timestampHeader = ટાઇમસ્ટેમ્પ
categoryHeader = શ્રેણી
methodHeader = પદ્ધતિ
objectHeader = ઑબ્જેક્ટ
extraHeader = વધારાની
