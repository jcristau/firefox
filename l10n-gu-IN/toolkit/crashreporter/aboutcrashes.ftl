# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = ભંગાણ અહેવાલો
clear-all-reports-label = બધા અહેવાલોને દૂર કરો
delete-confirm-title = શું તમે ચોક્કસ છો?
delete-confirm-description = આ બધા અહેવાલો કાઢી નાંખશે અને રદ કરી શકાશે નહિં
crashes-unsubmitted-label = સબમિટ ન થયેલા ક્રેશ રિપોર્ટ્સ
id-heading = અહેવાલ ID
date-crashed-heading = તારીખ ક્રેશ
crashes-submitted-label = ભંગાણ અહેવાલો જમા કર્યા
date-submitted-heading = જમા થયેલ તારીખ
no-reports-label = કોઈ ભંગાણ અહેવાલો જમા થયેલ નથી.
no-config-label = આ કાર્યક્રમ ભંગાણ અહેવાલો દર્શાવવા માટે રૂપરેખાંકિત થયેલ નથી. પસંદગી <code>breakpad.reportURL</code> સુયોજીત થયેલ હોવી જ જોઈએ.
