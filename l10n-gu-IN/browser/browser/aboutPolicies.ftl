# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = ઉદ્યોગ-સાહસ નીતિઓ
# 'Active' is used to describe the policies that are currently active
active-policies-tab = સક્રિય
errors-tab = ભૂલો
documentation-tab = દસ્તાવેજીકરણ
policy-name = નીતિનું નામ
policy-value = નીતિ મૂલ્ય
policy-errors = નીતિ ભૂલો
