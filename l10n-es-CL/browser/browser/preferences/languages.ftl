# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Idiomas
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Las páginas web a veces son ofrecidas en más de un idioma. Elige los idiomas para mostrar estas páginas, en orden de preferencia.
languages-customize-spoof-english =
    .label = Solicitar versiones en inglés de las páginas web para una privacidad mejorada
languages-customize-moveup =
    .label = Mover hacia arriba
    .accesskey = U
languages-customize-movedown =
    .label = Mover hacia abajo
    .accesskey = D
languages-customize-remove =
    .label = Eliminar
    .accesskey = r
languages-customize-select-language =
    .placeholder = Seleccione un idioma…
languages-customize-add =
    .label = Añadir
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
