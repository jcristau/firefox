# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Reportes de fallos
clear-all-reports-label = Eliminar todos los informes
delete-confirm-title = ¿Estás seguro?
delete-confirm-description = Esto borrará todos los informes y no se puede revertir.
crashes-unsubmitted-label = Informes de fallos no enviados
id-heading = ID del informe
date-crashed-heading = Fecha del fallo
crashes-submitted-label = Informes de fallos enviados
date-submitted-heading = Fecha de envío
no-reports-label = No se ha enviado ningún informe de fallos.
no-config-label = Esta aplicación no ha sido configurada para mostrar informes de fallos. Debe configurarse la preferencia <code>breakpad.reportURL</code>.
