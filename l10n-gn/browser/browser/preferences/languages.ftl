# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Ñe'ẽita ha tai mbopapapy
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Ñanduti kuatiarogue sapy'ánte oñekuave'ẽ ndaha'éi peteĩ ñe'ẽme añónte. Eiporavo ñe'ẽnguéra ojehecha hag̃ua ko'ã ñanduti kuatiarogue, jerohoryvéva rupi
languages-customize-spoof-english =
    .label = Tejejerure ñandutirogue oĩva ingléspe ikatuhag̃uáicha oñeñangarekove che rekovepypeguáre
languages-customize-moveup =
    .label = Jupi
    .accesskey = J
languages-customize-movedown =
    .label = Mboguejy
    .accesskey = M
languages-customize-remove =
    .label = Mboguete
    .accesskey = R
languages-customize-select-language =
    .placeholder = Eiporavo peteĩ ñe'ẽ mbojoapyrã…
languages-customize-add =
    .label = Mbojoapy
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
