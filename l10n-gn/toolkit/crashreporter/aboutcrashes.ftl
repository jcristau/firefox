# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Marandu jejavyguáva
clear-all-reports-label = Opavave momarandu mbogue
delete-confirm-title = Upéichapa reipota
delete-confirm-description = Ko jeku'e omboguepaitéta oĩva marandu'i ha ndaikatuvéima oñembojevyjey.
crashes-unsubmitted-label = Jejavy ñemondo momarandu
id-heading = ID momarandu
date-crashed-heading = Crashed arange
crashes-submitted-label = Jejavy ñemondo momarandu
date-submitted-heading = Ñemondo ára
no-reports-label = Ndojeguerahaukái jejavy momarandu.
no-config-label = Ko tembipuru'i noñembohekói ohechauka hag̃ua marandu'i jejavy rehegua. Oñembohekova'erã pe jerohoryvéva <code>breakpad.reportURL</code>.
