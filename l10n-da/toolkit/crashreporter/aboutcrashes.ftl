# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Fejlrapporter
clear-all-reports-label = Slet alle fejlrapporter
delete-confirm-title = Er du sikker?
delete-confirm-description = Dette vil slette alle fejlrapporter og kan ikke fortrydes.
crashes-unsubmitted-label = Ikke-sendte fejlrapporter
id-heading = Rapport-ID
date-crashed-heading = Nedbrudsdato
crashes-submitted-label = Sendte fejlrapporter
date-submitted-heading = Indsendelsesdato
no-reports-label = Du har ikke indsendt nogen fejlrapporter.
no-config-label = Dette program er ikke konfigureret til at vise fejlrapporter. Indstillingen <code>breakpad.reportURL</code> skal være sat.
