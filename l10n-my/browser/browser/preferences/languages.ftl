# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = ဘာသာစကားများ
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = ဝဘ်စာမျက်နှာများသည် တစ်ခါတစ်ရံ ဘာသာစကားတစ်ခုထက်ပို၍ အသုံးပြုခွင့်ပေးသည်။ ထိုဝဘ်စာမျက်နှာများကို ပြရန် ဘာသာစကားများကို အစဉ်အလိုက် ရွေးပါ။
languages-customize-spoof-english =
    .label = ကိုယ်ရေးလုံခြုံမှုအတွက် အင်္ဂလိပ်ဗားရှင်းဖြင့် ဝဘ်စာမျက်နှာကို တောင်းခံပါ
languages-customize-moveup =
    .label = အထက်သို့ ရွှေ့ပါ
    .accesskey = U
languages-customize-movedown =
    .label = အောက်သို့ ရွှေ့ပါ
    .accesskey = D
languages-customize-remove =
    .label = ဖယ်ရှားပါ
    .accesskey = R
languages-customize-select-language =
    .placeholder = ဘာသာစကားတစ်ခု ရွေးပါ…
languages-customize-add =
    .label = ထည့်ပါ
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
