# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Taq ch'ab'äl
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Jujun taq ruxaq wuj etz'ib'an pa jalajöj taq ch'ab'äl. Tacha' ri ch'ab'äl ütz yetz'et re taq ruxaq k'amaya'l re', achike na ri narayib'ej.
languages-customize-spoof-english =
    .label = Kek'utüx taq ruxaq ajk'amaya'l pa Q'anchi' richin nutziläx ri ichinanem
languages-customize-moveup =
    .label = Tijotob'äx
    .accesskey = j
languages-customize-movedown =
    .label = Tiqasäx
    .accesskey = T
languages-customize-remove =
    .label = Telesäx
    .accesskey = s
languages-customize-select-language =
    .placeholder = Tacha' jun ch'ab'äl ri nitz'aqatisäx…
languages-customize-add =
    .label = Titz'aqatisäx
    .accesskey = T
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
