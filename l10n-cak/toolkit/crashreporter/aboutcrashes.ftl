# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Rutzijol taq sachoj
clear-all-reports-label = Keyuj el konojel ri taq rutzijol
delete-confirm-title = ¿Kan at jikïl?
delete-confirm-description = Re jun samaj re' xkeruyüj el konojel ri taq rutzijol chuqa' man xtitikïr ta xtitzolin chi rij.
crashes-unsubmitted-label = Rutzijol taq sachoj etaqon
id-heading = Nimaläj rub'i' ri rutzijol
date-crashed-heading = Q'ij xqa ruchuq'a' q'inoj
crashes-submitted-label = Rutzijol taq sachoj etaqon
date-submitted-heading = Ruq'ijul toq xtaq
no-reports-label = Man etaqon ta ri taq rutzijol sachoj.
no-config-label = Re chokoy re man nuk'un richin yeruk'üt pe ri taq kitzijol taq sachoj. K'atzinel nijikib'äx ri rajowaxik <code>breakpad.reportURL</code>.
