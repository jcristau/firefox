# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = भाषा
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = वेब पृष्ठ कभी-कभी एकाधिक भाषाओं में प्रस्तुत किए जाते है. इनको प्रदर्शित करने के लिए वरीयता क्रम में भाषाओं को चुनें
languages-customize-spoof-english =
    .label = अधिक गोपनीयता हेतु वेब पृष्ठों के अंग्रेजी संस्करणों का अनुरोध करें
languages-customize-moveup =
    .label = ऊपर जाएँ
    .accesskey = U
languages-customize-movedown =
    .label = नीचे जाएँ
    .accesskey = D
languages-customize-remove =
    .label = हटाएँ
    .accesskey = R
languages-customize-select-language =
    .placeholder = भाषा जोड़ने के लिए चुनें...
languages-customize-add =
    .label = जोड़ें
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
