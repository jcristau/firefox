# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = 錯誤資訊報表
clear-all-reports-label = 移除所有報表
delete-confirm-title = 您確定嗎？
delete-confirm-description = 這會刪除所有報表且無法復原。
crashes-unsubmitted-label = 未送出的錯誤報告
id-heading = 報表編號
date-crashed-heading = 錯誤發生日期
crashes-submitted-label = 已送出的錯誤報告
date-submitted-heading = 送出日期
no-reports-label = 無錯誤資訊報表被送出。
no-config-label = 此程式尚未設定好以顯示錯誤資訊報表，必須設好 <code>breakpad.reportURL</code> 的值。
