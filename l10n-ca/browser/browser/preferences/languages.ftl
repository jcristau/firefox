# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Llengües
    .style = width: 35em
languages-close-key =
    .key = w
languages-description = A vegades les pàgines web s'ofereixen en més d'una llengua. Trieu les llengües per mostrar aquestes pàgines web, en ordre de preferència
languages-customize-spoof-english =
    .label = Sol·licita la versió anglesa de les pàgines web per millorar la privadesa
languages-customize-moveup =
    .label = Mou amunt
    .accesskey = M
languages-customize-movedown =
    .label = Mou avall
    .accesskey = v
languages-customize-remove =
    .label = Elimina
    .accesskey = E
languages-customize-select-language =
    .placeholder = Afegeix una llengua…
languages-customize-add =
    .label = Afegeix
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
