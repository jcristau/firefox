# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Språk
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Nettsider er av og til tilgjengelege i meir enn eitt språk. Velg kva for språk du vil vise nettsider i, i rekkjefølgja du føretrekkjer
languages-customize-spoof-english =
    .label = Be om engelske versjonar av nettsider for auka personvern
languages-customize-moveup =
    .label = Flytt opp
    .accesskey = o
languages-customize-movedown =
    .label = Flytt ned
    .accesskey = n
languages-customize-remove =
    .label = Fjern
    .accesskey = F
languages-customize-select-language =
    .placeholder = Vel eit språk å leggje til…
languages-customize-add =
    .label = Legg til
    .accesskey = L
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
