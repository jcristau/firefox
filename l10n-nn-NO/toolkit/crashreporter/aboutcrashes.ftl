# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Krasjrapportar
clear-all-reports-label = Slett alle rapportar
delete-confirm-title = Er du sikker?
delete-confirm-description = Dette vil slette alle rapportane og kan ikkje gjerast om.
crashes-unsubmitted-label = Ikkje-innsende krasjrapportar
id-heading = Rapport-ID
date-crashed-heading = Dato for krasj
crashes-submitted-label = Innsende krasjrapportar
date-submitted-heading = Dato sendt
no-reports-label = Ingen krasjrapportar er sende inn.
no-config-label = Dette programmet er ikkje sett opp for å vise krasjrapportar. Innstillinga <code>breakpad.reportURL</code> må veljast.
