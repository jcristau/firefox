# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Сообщения о падениях
clear-all-reports-label = Удалить все сообщения
delete-confirm-title = Вы уверены?
delete-confirm-description = Это удалит все сообщения и не может быть отменено.
crashes-unsubmitted-label = Неотправленные сообщения о падениях
id-heading = Идентификатор сообщения
date-crashed-heading = Дата падения
crashes-submitted-label = Отправленные сообщения о падениях
date-submitted-heading = Дата отправки
no-reports-label = Ни одного сообщения о падении отправлено не было.
no-config-label = Это приложение не было настроено на отображение сообщений о падениях. Необходимо установить параметр <code>breakpad.reportURL</code>.
