<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "Ynghylch Developer Tools">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Galluogi Firefox Developer Tools">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "Galluogi Firefox Developer Tools i ddefnyddio Inspect Element">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "Archwilio a golygu HTML a CSS gyda'r Archwiliwr Developer Tools.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "Datblygu a dadfygio WebExtensions, gweithwyr gwe, gweithwyr gwasanaeth a mwy gyda Firefox Developer Tools.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "Rydych wedi cychwyn llwybr byr Developer Tools. Os oedd hynny'n gamgymeriad, gallwch gau'r tab hwn.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage
          "Archwilio, golygu a dadfygio HTML, CSS, a JavaScript gydag offer fel yr Archwiliwr a'r Dadfygiwr.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "Perffeithio HTML, CSS, a JavaScript eich gwefan gydag offer fel yr Archwiliwr a'r Dadfygiwr.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "Mae'r Firefox Developer Tools wedi ei analluogi yn rhagosodedig i roi mwy o reolaeth i chi dros eich porwr.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "Dysgu rhagor am Developer Tools">

<!ENTITY  aboutDevtools.enable.enableButton "Galluogi Developer Tools">
<!ENTITY  aboutDevtools.enable.closeButton "Cau'r dudalen hon">

<!ENTITY  aboutDevtools.enable.closeButton2 "Cau'r Tab hwn">

<!ENTITY  aboutDevtools.welcome.title "Croeso i Firefox Developer Tools!">

<!ENTITY  aboutDevtools.newsletter.title "Mozilla Developer Newsletter">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "Derbyn newyddion, triciau ac adnoddau ar gyfer datblygwyr yn syth i'ch blwch derbyn.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "E-bost">
<!ENTITY  aboutDevtools.newsletter.privacy.label "Rwy'n fodlon i Mozilla drin fy manylion fel mae'n cael ei esbonio yn <a class='external' href='https://www.mozilla.org/privacy/'>y Polisi Preifatrwydd hwn</a>.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "Tanysgrifio">
<!ENTITY  aboutDevtools.newsletter.thanks.title "Diolch!">
<!ENTITY  aboutDevtools.newsletter.thanks.message "Os nad ydych eisoes wedi cadarnhau tanysgrifiad i gylchlythyr yn perthyn i Mozilla, mae'n bosib y bydd yn rhaid i chi wneud hynny. Gwiriwch eich blwch derbyn neu eich hidl sbam am e-bost gennym ni.">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "Chwilio am fwy na dim ond Developer Tools? Edrychwch ar y porwr Firefox sydd wedi ei adeiladu yn benodol ar gyfer datblygwyr a'r llif gwaith modern.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "Dysgu rhagor">

