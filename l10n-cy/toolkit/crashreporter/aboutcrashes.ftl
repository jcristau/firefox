# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Adroddiadau Chwalu
clear-all-reports-label = Tynnu pob Adroddiad
delete-confirm-title = Ydych chi'n siŵr?
delete-confirm-description = Bydd hyn yn dileu pob adroddiad ac nid oes modd ei ddadwneud.
crashes-unsubmitted-label = Adroddiadau Chwalu heb eu Cyflwyno
id-heading = Enw'r Adroddiad
date-crashed-heading = Dyddiad y Chwalu
crashes-submitted-label = Adroddiadau Chwalu
date-submitted-heading = Dyddiad Cyflwynwyd
no-reports-label = Does dim adroddiadau chwalu wedi eu cyflwyno.
no-config-label = Mae'r rhaglen wedi ei ffurfweddu i ddangos adroddiadau chwalu. Rhaid gosod y <code>breakpad.reportURL</code>.
