# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Hibajelentések
clear-all-reports-label = Minden jelentés törlése
delete-confirm-title = Biztos benne?
delete-confirm-description = A művelet az összes jelentést törli, és nem lehet visszavonni.
crashes-unsubmitted-label = Be nem küldött hibajelentések
id-heading = Jelentésazonosító
date-crashed-heading = Összeomlás dátuma
crashes-submitted-label = Beküldött jelentések az összeomlásokról
date-submitted-heading = Beküldés dátuma
no-reports-label = Nem volt még beküldve jelentés.
no-config-label = Ez az alkalmazás nincs az összeomlási jelentések megjelenítésére beállítva. Be kell állítani a <code>breakpad.reportURL</code> paramétert.
