# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (saveAddressesMessage): %S is brandShortName. This string is used on the doorhanger to
# notify users that addresses are saved.
saveAddressesMessage = A %S ettől kezdve elmenti a címeket, hogy gyorsabban tölthesse ki az űrlapokat.
# LOCALIZATION NOTE (autofillOptionsLink, autofillOptionsLinkOSX): These strings are used in the doorhanger for
# updating addresses. The link leads users to Form Autofill browser preferences.
autofillOptionsLink = Automatikus űrlapkitöltési beállítások
autofillOptionsLinkOSX = Automatikus űrlapkitöltési beállítások
# LOCALIZATION NOTE (autofillSecurityOptionsLink, autofillSecurityOptionsLinkOSX): These strings are used
# in the doorhanger for saving credit card info. The link leads users to Form Autofill browser preferences.
autofillSecurityOptionsLink = Automatikus űrlapkitöltési és biztonsági beállítások
autofillSecurityOptionsLinkOSX = Automatikus űrlapkitöltési és biztonsági beállítások
# LOCALIZATION NOTE (changeAutofillOptions, changeAutofillOptionsOSX): These strings are used on the doorhanger
# that notifies users that addresses are saved. The button leads users to Form Autofill browser preferences.
changeAutofillOptions = Automatikus űrlapkitöltési beállítások megváltoztatása
changeAutofillOptionsOSX = Automatikus űrlapkitöltési beállítások megváltoztatása
changeAutofillOptionsAccessKey = v
# LOCALIZATION NOTE (addressesSyncCheckbox): If Sync is enabled, this checkbox is displayed on the doorhanger
# shown when saving addresses.
addressesSyncCheckbox = Címek megosztása a szinkronizált eszközök között
# LOCALIZATION NOTE (creditCardsSyncCheckbox): If Sync is enabled and credit card sync is available,
# this checkbox is displayed on the doorhanger shown when saving credit card.
creditCardsSyncCheckbox = Bankkártyák megosztása a szinkronizált eszközök között
# LOCALIZATION NOTE (updateAddressMessage, updateAddressDescriptionLabel, createAddressLabel, updateAddressLabel):
# Used on the doorhanger when an address change is detected.
updateAddressMessage = Szeretné frissíteni a címét ezekkel az új információkkal?
updateAddressDescriptionLabel = Frissítendő cím:
createAddressLabel = Új cím létrehozása
createAddressAccessKey = l
updateAddressLabel = Cím frissítése
updateAddressAccessKey = f
# LOCALIZATION NOTE (saveCreditCardMessage, saveCreditCardDescriptionLabel, saveCreditCardLabel, cancelCreditCardLabel, neverSaveCreditCardLabel):
# Used on the doorhanger when users submit payment with credit card.
# LOCALIZATION NOTE (saveCreditCardMessage): %S is brandShortName.
saveCreditCardMessage = Szeretné, hogy a %S elmentse ezt a bankkártyát? (A biztonsági kódot nem fogja elmenteni)
saveCreditCardDescriptionLabel = Mentendő bankkártya:
saveCreditCardLabel = Bankkártya mentése
saveCreditCardAccessKey = m
cancelCreditCardLabel = Ne mentse
cancelCreditCardAccessKey = N
neverSaveCreditCardLabel = Sose mentse el a bankkártyákat
neverSaveCreditCardAccessKey = S
# LOCALIZATION NOTE (updateCreditCardMessage, updateCreditCardDescriptionLabel, createCreditCardLabel, updateCreditCardLabel):
# Used on the doorhanger when an credit card change is detected.
updateCreditCardMessage = Szeretné frissíteni a bankkártyáját ezekkel az új információkkal?
updateCreditCardDescriptionLabel = Frissítendő bankkártya:
createCreditCardLabel = Új bankkártya létrehozása
createCreditCardAccessKey = j
updateCreditCardLabel = Bankkártya frissítése
updateCreditCardAccessKey = f
# LOCALIZATION NOTE (openAutofillMessagePanel): Tooltip label for Form Autofill doorhanger icon on address bar.
openAutofillMessagePanel = Űrlapkitöltési üzenetpanel megnyitása

# LOCALIZATION NOTE ( (autocompleteFooterOptionShort, autocompleteFooterOptionOSXShort): Used as a label for the button,
# displayed at the bottom of the dropdown suggestion, to open Form Autofill browser preferences.
autocompleteFooterOptionShort = További beállítások
autocompleteFooterOptionOSXShort = Beállítások
# LOCALIZATION NOTE (category.address, category.name, category.organization2, category.tel, category.email):
# Used in autofill drop down suggestion to indicate what other categories Form Autofill will attempt to fill.
category.address = cím
category.name = név
category.organization2 = szervezet
category.tel = telefonszám
category.email = e-mail
# LOCALIZATION NOTE (fieldNameSeparator): This is used as a separator between categories.
fieldNameSeparator = ,\u0020
# LOCALIZATION NOTE (phishingWarningMessage, phishingWarningMessage2): The warning
# text that is displayed for informing users what categories are about to be filled.
# "%S" will be replaced with a list generated from the pre-defined categories.
# The text would be e.g. Also autofills organization, phone, email.
phishingWarningMessage = Automatikusan kitölti ezeket is: %S
phishingWarningMessage2 = Automatikusan kitölti: %S
# LOCALIZATION NOTE (insecureFieldWarningDescription): %S is brandShortName. This string is used in drop down
# suggestion when users try to autofill credit card on an insecure website (without https).
insecureFieldWarningDescription = A %S nem biztonságos oldalt észlelt. Az automatikus űrlapkitöltés ideiglenesen letiltva
# LOCALIZATION NOTE (clearFormBtnLabel2): Label for the button in the dropdown menu that used to clear the populated
# form.
clearFormBtnLabel2 = Automatikus kitöltött űrlap ürítése

# LOCALIZATION NOTE (autofillAddressesCheckbox): Label for the checkbox that enables autofilling addresses.
autofillAddressesCheckbox = Címek automatikus kitöltése
# LOCALIZATION NOTE (learnMoreLabel): Label for the link that leads users to the Form Autofill SUMO page.
learnMoreLabel = További tudnivalók
# LOCALIZATION NOTE (savedAddressesBtnLabel): Label for the button that opens a dialog that shows the
# list of saved addresses.
savedAddressesBtnLabel = Mentett címek…
# LOCALIZATION NOTE (autofillCreditCardsCheckbox): Label for the checkbox that enables autofilling credit cards.
autofillCreditCardsCheckbox = Bankkártyaadatok automatikus kitöltése
# LOCALIZATION NOTE (savedCreditCardsBtnLabel): Label for the button that opens a dialog that shows the list
# of saved credit cards.
savedCreditCardsBtnLabel = Mentett bankkártyák…

# LOCALIZATION NOTE (manageAddressesTitle, manageCreditCardsTitle): The dialog title for the list of addresses or
# credit cards in browser preferences.
manageAddressesTitle = Mentett címek
manageCreditCardsTitle = Mentett bankkártyák
# LOCALIZATION NOTE (addressesListHeader, creditCardsListHeader): The header for the list of addresses or credit cards
# in browser preferences.
addressesListHeader = Címek
creditCardsListHeader = Bankkártyák
showCreditCardsBtnLabel = Bankkártyák megjelenítése
hideCreditCardsBtnLabel = Bankkártyák elrejtése
removeBtnLabel = Eltávolítás
addBtnLabel = Hozzáadás…
editBtnLabel = Szerkesztés…
# LOCALIZATION NOTE (manageDialogsWidth): This strings sets the default width for windows used to manage addresses and
# credit cards.
manageDialogsWidth = 560px

# LOCALIZATION NOTE (addNewAddressTitle, editAddressTitle): The dialog title for creating or editing addresses
# in browser preferences.
addNewAddressTitle = Új cím hozzáadása
editAddressTitle = Cím szerkesztése
givenName = Utónév
additionalName = Egyéb név
familyName = Vezetéknév
organization2 = Szervezet
streetAddress = Utca, hászszám
city = Város
province = Tartomány
state = Állam
postalCode = Irányítószám
zip = Irányítószám (Amerikai Egyesült Államok)
country = Ország vagy régió
tel = Telefonszám
email = E-mail
cancelBtnLabel = Mégse
saveBtnLabel = Mentés
countryWarningMessage2 = Az űrlapkitöltés jelenleg csak egyes országbeli címekre érhető el.

# LOCALIZATION NOTE (addNewCreditCardTitle, editCreditCardTitle): The dialog title for creating or editing
# credit cards in browser preferences.
addNewCreditCardTitle = Új bankkártya hozzáadása
editCreditCardTitle = Bankkártya szerkesztése
cardNumber = Kártyaszám
invalidCardNumber = Írjon be érvényes kártyaszámot
nameOnCard = Kártyán szereplő név
cardExpires = Lejárat
cardExpiresMonth = Lejárat hónapja
cardExpiresYear = Lejárat éve
billingAddress = Számlázási cím
