# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (saveAddressesMessage): %S is brandShortName. This string is used on the doorhanger to
# notify users that addresses are saved.
saveAddressesMessage = %S umí ukládat adresy a rychle je pak za vás vyplnit do potřebných formulářů.
# LOCALIZATION NOTE (autofillOptionsLink, autofillOptionsLinkOSX): These strings are used in the doorhanger for
# updating addresses. The link leads users to Form Autofill browser preferences.
autofillOptionsLink = Možnosti automatického vyplňování formulářů
autofillOptionsLinkOSX = Předvolby automatického vyplňování formulářů
# LOCALIZATION NOTE (autofillSecurityOptionsLink, autofillSecurityOptionsLinkOSX): These strings are used
# in the doorhanger for saving credit card info. The link leads users to Form Autofill browser preferences.
autofillSecurityOptionsLink = Možnosti zabezpečení automatického vyplňování formulářů
autofillSecurityOptionsLinkOSX = Předvolby zabezpečení automatického vyplňování formulářů
# LOCALIZATION NOTE (changeAutofillOptions, changeAutofillOptionsOSX): These strings are used on the doorhanger
# that notifies users that addresses are saved. The button leads users to Form Autofill browser preferences.
changeAutofillOptions = Nastavit automatické vyplňování formulářů
changeAutofillOptionsOSX = Nastavit automatické vyplňování formulářů
changeAutofillOptionsAccessKey = c
# LOCALIZATION NOTE (addressesSyncCheckbox): If Sync is enabled, this checkbox is displayed on the doorhanger
# shown when saving addresses.
addressesSyncCheckbox = Synchronizovat adresy mezi vašimi zařízeními
# LOCALIZATION NOTE (creditCardsSyncCheckbox): If Sync is enabled and credit card sync is available,
# this checkbox is displayed on the doorhanger shown when saving credit card.
creditCardsSyncCheckbox = Synchronizovat platební karty mezi vašimi zařízeními
# LOCALIZATION NOTE (updateAddressMessage, updateAddressDescriptionLabel, createAddressLabel, updateAddressLabel):
# Used on the doorhanger when an address change is detected.
updateAddressMessage = Chcete doplnit vaši adresu o tyto nové informace?
updateAddressDescriptionLabel = Adresa, kterou chcete aktualizovat:
createAddressLabel = Vytvořit novou adresu
createAddressAccessKey = V
updateAddressLabel = Aktualizovat adresu
updateAddressAccessKey = A
# LOCALIZATION NOTE (saveCreditCardMessage, saveCreditCardDescriptionLabel, saveCreditCardLabel, cancelCreditCardLabel, neverSaveCreditCardLabel):
# Used on the doorhanger when users submit payment with credit card.
# LOCALIZATION NOTE (saveCreditCardMessage): %S is brandShortName.
saveCreditCardMessage = Má %S uložit informace o této platební kartě? (Bezpečnostní kód uložen nebude)
saveCreditCardDescriptionLabel = Platební karta, kterou chcete uložit:
saveCreditCardLabel = Uložit informace o kartě
saveCreditCardAccessKey = U
cancelCreditCardLabel = Neukládat
cancelCreditCardAccessKey = N
neverSaveCreditCardLabel = Nikdy neukládat informace o kartách
neverSaveCreditCardAccessKey = i
# LOCALIZATION NOTE (updateCreditCardMessage, updateCreditCardDescriptionLabel, createCreditCardLabel, updateCreditCardLabel):
# Used on the doorhanger when an credit card change is detected.
updateCreditCardMessage = Chcete doplnit informace o vaší platební kartě o tyto nové informace?
updateCreditCardDescriptionLabel = Platební karta, kterou chcete aktualizovat:
createCreditCardLabel = Vytvořit novou platební kartu
createCreditCardAccessKey = V
updateCreditCardLabel = Aktualizovat informace o platební kartě
updateCreditCardAccessKey = A
# LOCALIZATION NOTE (openAutofillMessagePanel): Tooltip label for Form Autofill doorhanger icon on address bar.
openAutofillMessagePanel = Otevře lištu automatického vyplňování formulářů

# LOCALIZATION NOTE ( (autocompleteFooterOptionShort, autocompleteFooterOptionOSXShort): Used as a label for the button,
# displayed at the bottom of the dropdown suggestion, to open Form Autofill browser preferences.
autocompleteFooterOptionShort = Více možností
autocompleteFooterOptionOSXShort = Předvolby
# LOCALIZATION NOTE (category.address, category.name, category.organization2, category.tel, category.email):
# Used in autofill drop down suggestion to indicate what other categories Form Autofill will attempt to fill.
category.address = adresu
category.name = jméno
category.organization2 = společnost
category.tel = telefon
category.email = e-mailovou adresu
# LOCALIZATION NOTE (fieldNameSeparator): This is used as a separator between categories.
fieldNameSeparator = ,\u0020
# LOCALIZATION NOTE (phishingWarningMessage, phishingWarningMessage2): The warning
# text that is displayed for informing users what categories are about to be filled.
# "%S" will be replaced with a list generated from the pre-defined categories.
# The text would be e.g. Also autofills organization, phone, email.
phishingWarningMessage = Také vyplní %S
phishingWarningMessage2 = Automaticky vyplní %S
# LOCALIZATION NOTE (insecureFieldWarningDescription): %S is brandShortName. This string is used in drop down
# suggestion when users try to autofill credit card on an insecure website (without https).
insecureFieldWarningDescription = Aplikace %S rozpoznala nezabezpečenou stránku. Automatické vyplňování formulářů je dočasně zakázáno.
# LOCALIZATION NOTE (clearFormBtnLabel2): Label for the button in the dropdown menu that used to clear the populated
# form.
clearFormBtnLabel2 = Vymazat automaticky vyplněný formulář

# LOCALIZATION NOTE (autofillAddressesCheckbox): Label for the checkbox that enables autofilling addresses.
autofillAddressesCheckbox = Automaticky vyplňovat adresy
# LOCALIZATION NOTE (learnMoreLabel): Label for the link that leads users to the Form Autofill SUMO page.
learnMoreLabel = Zjistit více
# LOCALIZATION NOTE (savedAddressesBtnLabel): Label for the button that opens a dialog that shows the
# list of saved addresses.
savedAddressesBtnLabel = Uložené adresy…
# LOCALIZATION NOTE (autofillCreditCardsCheckbox): Label for the checkbox that enables autofilling credit cards.
autofillCreditCardsCheckbox = Automaticky vyplňovat informace o platební kartě
# LOCALIZATION NOTE (savedCreditCardsBtnLabel): Label for the button that opens a dialog that shows the list
# of saved credit cards.
savedCreditCardsBtnLabel = Uložené platební karty…

# LOCALIZATION NOTE (manageAddressesTitle, manageCreditCardsTitle): The dialog title for the list of addresses or
# credit cards in browser preferences.
manageAddressesTitle = Uložené adresy
manageCreditCardsTitle = Uložené platební karty
# LOCALIZATION NOTE (addressesListHeader, creditCardsListHeader): The header for the list of addresses or credit cards
# in browser preferences.
addressesListHeader = Adresy
creditCardsListHeader = Platební karty
showCreditCardsBtnLabel = Zobrazit platební karty
hideCreditCardsBtnLabel = Skrýt platební karty
removeBtnLabel = Odstranit
addBtnLabel = Přidat…
editBtnLabel = Upravit…
# LOCALIZATION NOTE (manageDialogsWidth): This strings sets the default width for windows used to manage addresses and
# credit cards.
manageDialogsWidth = 560px

# LOCALIZATION NOTE (addNewAddressTitle, editAddressTitle): The dialog title for creating or editing addresses
# in browser preferences.
addNewAddressTitle = Přidat novou adresu
editAddressTitle = Upravit adresu
givenName = Křestní jméno
additionalName = Prostřední jméno
familyName = Příjmení
organization2 = Společnost
streetAddress = Ulice
city = Město
province = Kraj
state = Země
postalCode = PSČ
zip = PSČ
country = Země nebo oblast
tel = Telefon
email = E-mailová adresa
cancelBtnLabel = Zrušit
saveBtnLabel = Uložit
countryWarningMessage2 = Automatické vyplňování formulářů nyní funguje jenom pro některé země.

# LOCALIZATION NOTE (addNewCreditCardTitle, editCreditCardTitle): The dialog title for creating or editing
# credit cards in browser preferences.
addNewCreditCardTitle = Přidat novou platební kartu
editCreditCardTitle = Upravit informace o platební kartě
cardNumber = Číslo karty
invalidCardNumber = Zadejte prosím platné číslo platební karty
nameOnCard = Jméno na kartě
cardExpires = Platnost do
billingAddress = Fakturační adresa
