# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Jazyky
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Webové stránky jsou někdy k dispozici v několika jazycích. Zvolte jazyky v takovém pořadí, v jakém se mají používat pro zobrazování webových stránek
languages-customize-spoof-english =
    .label = Požadovat anglické verze webových stránek pro vyšší úroveň soukromí
languages-customize-moveup =
    .label = Posunout výše
    .accesskey = u
languages-customize-movedown =
    .label = Posunout níže
    .accesskey = n
languages-customize-remove =
    .label = Odebrat
    .accesskey = r
languages-customize-select-language =
    .placeholder = Zvolte jazyk…
languages-customize-add =
    .label = Přidat
    .accesskey = a
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale } [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
