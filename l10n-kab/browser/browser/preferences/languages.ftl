# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Tutlayin
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Kra n yisebtar web ttunefkayen-d deg waṭas n tutlayin. Fren tutlayin iwakken ad tsekneḍ isebtar-a akken i tesmenyafeḍ
languages-customize-spoof-english =
    .label = Suter isebtar web s tegilizit iwakken ad twenneɛ tbaḍnit-ik
languages-customize-moveup =
    .label = Sali
    .accesskey = S
languages-customize-movedown =
    .label = Ader
    .accesskey = A
languages-customize-remove =
    .label = Kkes
    .accesskey = K
languages-customize-select-language =
    .placeholder = Fren tutlayt ara ternuḍ...
languages-customize-add =
    .label = Rnu
    .accesskey = R
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
