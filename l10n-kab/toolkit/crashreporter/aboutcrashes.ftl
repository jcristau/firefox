# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Ineqqisen n uɣelluy
clear-all-reports-label = Kkes akk issaɣen
delete-confirm-title = Asentem?
delete-confirm-description = Ad yekkes akk issaɣen s war tuɣalin.
crashes-unsubmitted-label = Ineqqisen n uɣelluy ur yettwaznen ara
id-heading = Asulay n ussaɣ
date-crashed-heading = Azemz n uɣelluy
crashes-submitted-label = Ineqqisen n uɣelluy ttwaznen.
date-submitted-heading = Azemz n tuzzna
no-reports-label = Ula d yiwen n ussaɣ n uɣelluy ur iţwazen.
no-config-label = Asnas-agi ur ittusbadu ara akken ad isken ineqqisen n uɣelluy. Issefk ad tbaduḍ imetti <code>breakpad.reportURL</code>.
