# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Lengoe
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Quarche vòtta e pagine gh'an ciù de 'na lengoa. L'é poscibile çerne na lengoa predefinia pe ste pagine, in ordine de preferensa
languages-customize-spoof-english =
    .label = Serve a verscion ingleize de pagine pe avei a privacy avansâ
languages-customize-moveup =
    .label = Mescia in sciù
    .accesskey = M
languages-customize-movedown =
    .label = Mescia in zu
    .accesskey = M
languages-customize-remove =
    .label = Scancella
    .accesskey = S
languages-customize-select-language =
    .placeholder = Seleçionn-a 'na lengoa pe azonzila…
languages-customize-add =
    .label = Azonzi
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale } [{ $code }]
languages-active-code-format =
    .value = { languages-code-format.label }
