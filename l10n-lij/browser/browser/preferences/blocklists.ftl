# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Elenchi pe blòcco elementi che tracian
    .style = width: 55em
blocklist-desc = Çerni l’elenco da aduviâ in { -brand-short-name } pe blocâ i elementi che peuan traciâ a navegaçion.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Elenco
blocklist-button-cancel =
    .label = Anulla
    .accesskey = A
blocklist-button-ok =
    .label = Sarva modifiche
    .accesskey = S
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Disconnect.me proteçion baze (consegiou).
blocklist-item-moz-std-desc = Permette quarche elemento ch'o tracia pe garantî o bon fonçionamento di sciti.
blocklist-item-moz-full-name = Disconnect.me proteçion mascima.
blocklist-item-moz-full-desc = Blòcca i elementi che tracian conosciui. Quarche scito o porieiva no fonçionâ ben.
