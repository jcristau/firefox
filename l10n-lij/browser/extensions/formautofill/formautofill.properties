# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (saveAddressesMessage): %S is brandShortName. This string is used on the doorhanger to
# notify users that addresses are saved.
saveAddressesMessage = %S o sarva i indirissi coscì ti peu inpî e forme ciù spedio.
# LOCALIZATION NOTE (autofillOptionsLink, autofillOptionsLinkOSX): These strings are used in the doorhanger for
# updating addresses. The link leads users to Form Autofill browser preferences.
autofillOptionsLink = Inpostaçioin de aoto-inpimento de form
autofillOptionsLinkOSX = Inpostaçioin de aoto-inpimento de form
# LOCALIZATION NOTE (autofillSecurityOptionsLink, autofillSecurityOptionsLinkOSX): These strings are used
# in the doorhanger for saving credit card info. The link leads users to Form Autofill browser preferences.
autofillSecurityOptionsLink = Inpostaçioin de aoto-inpimento form e seguessa
autofillSecurityOptionsLinkOSX = Inpostaçioin de aoto-inpimento form e seguessa
# LOCALIZATION NOTE (changeAutofillOptions, changeAutofillOptionsOSX): These strings are used on the doorhanger
# that notifies users that addresses are saved. The button leads users to Form Autofill browser preferences.
changeAutofillOptions = Cangia e inpostaçioin de aoto-inpimento de form
changeAutofillOptionsOSX = Cangia e inpostaçioin de aoto-inpimento de form
changeAutofillOptionsAccessKey = C
# LOCALIZATION NOTE (addressesSyncCheckbox): If Sync is enabled, this checkbox is displayed on the doorhanger
# shown when saving addresses.
addressesSyncCheckbox = Condividdi indirssi co-i dispoxitivi scincronizæ
# LOCALIZATION NOTE (creditCardsSyncCheckbox): If Sync is enabled and credit card sync is available,
# this checkbox is displayed on the doorhanger shown when saving credit card.
creditCardsSyncCheckbox = Condividdi carte a poffo co-i dispoxitivi scincronizæ
# LOCALIZATION NOTE (updateAddressMessage, updateAddressDescriptionLabel, createAddressLabel, updateAddressLabel):
# Used on the doorhanger when an address change is detected.
updateAddressMessage = Ti veu agiornâ e teu infromaçioin in sciô teu indirisso con sta neuva informaçion?
updateAddressDescriptionLabel = Indirisso da agiornâ:
createAddressLabel = Crea neuvo indirisso
createAddressAccessKey = C
updateAddressLabel = Agiorna indirisso
updateAddressAccessKey = A
# LOCALIZATION NOTE (saveCreditCardMessage, saveCreditCardDescriptionLabel, saveCreditCardLabel, cancelCreditCardLabel, neverSaveCreditCardLabel):
# Used on the doorhanger when users submit payment with credit card.
# LOCALIZATION NOTE (saveCreditCardMessage): %S is brandShortName.
saveCreditCardMessage = Ti veu che %S o sarve sta carta a poffo? (O còdice de seguessa o no saiâ sarvou)
saveCreditCardDescriptionLabel = Carta a poffo da sarvâ:
saveCreditCardLabel = Sarva carta a poffo
saveCreditCardAccessKey = S
cancelCreditCardLabel = No sarvâ
cancelCreditCardAccessKey = N
neverSaveCreditCardLabel = No sarvâ mai Carte a poffo
neverSaveCreditCardAccessKey = N
# LOCALIZATION NOTE (updateCreditCardMessage, updateCreditCardDescriptionLabel, createCreditCardLabel, updateCreditCardLabel):
# Used on the doorhanger when an credit card change is detected.
updateCreditCardMessage = Ti veu agiornâ e teu infromaçioin in sciâ teu carta a poffo con sta neuva informaçion?
updateCreditCardDescriptionLabel = Carta a poffo agiornæ:
createCreditCardLabel = Crea neuva Carta a poffo
createCreditCardAccessKey = C
updateCreditCardLabel = Agiorna Carta a poffo
updateCreditCardAccessKey = A
# LOCALIZATION NOTE (openAutofillMessagePanel): Tooltip label for Form Autofill doorhanger icon on address bar.
openAutofillMessagePanel = Arvi o panello de Form aoto-inpimento

# LOCALIZATION NOTE ( (autocompleteFooterOptionShort, autocompleteFooterOptionOSXShort): Used as a label for the button,
# displayed at the bottom of the dropdown suggestion, to open Form Autofill browser preferences.
autocompleteFooterOptionShort = Ciù inpostaçioin
autocompleteFooterOptionOSXShort = Preferense
# LOCALIZATION NOTE (category.address, category.name, category.organization2, category.tel, category.email):
# Used in autofill drop down suggestion to indicate what other categories Form Autofill will attempt to fill.
category.address = indirisso
category.name = nomme
category.organization2 = òrganizaçion
category.tel = telefono
category.email = email
# LOCALIZATION NOTE (fieldNameSeparator): This is used as a separator between categories.
fieldNameSeparator = ,\u0020
# LOCALIZATION NOTE (phishingWarningMessage, phishingWarningMessage2): The warning
# text that is displayed for informing users what categories are about to be filled.
# "%S" will be replaced with a list generated from the pre-defined categories.
# The text would be e.g. Also autofills organization, phone, email.
phishingWarningMessage = Aoto-inpi anche %S
phishingWarningMessage2 = Aoto-inpi %S
# LOCALIZATION NOTE (insecureFieldWarningDescription): %S is brandShortName. This string is used in drop down
# suggestion when users try to autofill credit card on an insecure website (without https).
insecureFieldWarningDescription = %S o l'à trovou 'n scito no seguo. E form aoto-inpimento o l'é dizabilitou tenporaneamente.
# LOCALIZATION NOTE (clearFormBtnLabel2): Label for the button in the dropdown menu that used to clear the populated
# form.
clearFormBtnLabel2 = Scancella Form aoto-inpimento

# LOCALIZATION NOTE (autofillAddressesCheckbox): Label for the checkbox that enables autofilling addresses.
autofillAddressesCheckbox = Aoto-inpi indirissi
# LOCALIZATION NOTE (learnMoreLabel): Label for the link that leads users to the Form Autofill SUMO page.
learnMoreLabel = Atre informaçioin
# LOCALIZATION NOTE (savedAddressesBtnLabel): Label for the button that opens a dialog that shows the
# list of saved addresses.
savedAddressesBtnLabel = Indirissi sarvæ…
# LOCALIZATION NOTE (autofillCreditCardsCheckbox): Label for the checkbox that enables autofilling credit cards.
autofillCreditCardsCheckbox = Aoto-inpi Carte a poffo
# LOCALIZATION NOTE (savedCreditCardsBtnLabel): Label for the button that opens a dialog that shows the list
# of saved credit cards.
savedCreditCardsBtnLabel = Carte a poffo sarvæ…

# LOCALIZATION NOTE (manageAddressesTitle, manageCreditCardsTitle): The dialog title for the list of addresses or
# credit cards in browser preferences.
manageAddressesTitle = Sarva indirissi
manageCreditCardsTitle = Carte a poffo sarvæ
# LOCALIZATION NOTE (addressesListHeader, creditCardsListHeader): The header for the list of addresses or credit cards
# in browser preferences.
addressesListHeader = Indirissi
creditCardsListHeader = Carte a poffo
showCreditCardsBtnLabel = Fanni vedde Carte a poffo
hideCreditCardsBtnLabel = Ascondi Carte a poffo
removeBtnLabel = Scancella
addBtnLabel = Azonzi…
editBtnLabel = Cangia…
# LOCALIZATION NOTE (manageDialogsWidth): This strings sets the default width for windows used to manage addresses and
# credit cards.
manageDialogsWidth = 560px

# LOCALIZATION NOTE (addNewAddressTitle, editAddressTitle): The dialog title for creating or editing addresses
# in browser preferences.
addNewAddressTitle = Azonzi neuvo indirisso
editAddressTitle = Cangia indirisso
givenName = Nomme
additionalName = Segondo nomme
familyName = Cognomme
organization2 = Òrganizaçion
streetAddress = Indirisso
city = Çitæ
province = Provinsa
state = Stato
postalCode = CAP
zip = Còdice ZIP
country = Tæra ò Region
tel = Telefono
email = Email
cancelBtnLabel = Anulla
saveBtnLabel = Sarva
countryWarningMessage2 = E form aoto-inpimento en disponibili solo in çerte naçioin.

# LOCALIZATION NOTE (addNewCreditCardTitle, editCreditCardTitle): The dialog title for creating or editing
# credit cards in browser preferences.
addNewCreditCardTitle = Azonzi neuva Carta a poffo
editCreditCardTitle = Cangia Carta a poffo
cardNumber = Numero carta
nameOnCard = Nomme in sciâ carta
cardExpires = Scadensa
billingAddress = Indirisso de fatuaçion
