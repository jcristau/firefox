# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Segnalaçion di Cianti
clear-all-reports-label = Leva da mezo tutti i Report
delete-confirm-title = T'ê seguo?
delete-confirm-description = Questo o scanceliâ tutto e o no se poriâ anula.
crashes-unsubmitted-label = Report di cianti no mandæ
id-heading = ID do Report
date-crashed-heading = Dæta do cianto
crashes-submitted-label = Report di cianti mandæ
date-submitted-heading = Dæta segnalaçion
no-reports-label = Nisciun report mandou.
no-config-label = Sta aplicaçion a no l'é stæta inpostâ pe fâ vedde i report di problemi. E preferense <code>breakpad.reportURL</code> son da inposta.
