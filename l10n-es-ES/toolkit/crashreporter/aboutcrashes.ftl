# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Informes de fallos
clear-all-reports-label = Eliminar todos los informes
delete-confirm-title = ¿Está seguro?
delete-confirm-description = Esta acción eliminará todos los informes y no puede deshacerse.
crashes-unsubmitted-label = Informes de fallos no enviados
id-heading = ID del informe
date-crashed-heading = Fecha del fallo
crashes-submitted-label = Informes de fallos enviados
date-submitted-heading = Fecha del envío
no-reports-label = No se han enviado informes de fallo.
no-config-label = Esta aplicación no ha sido configurada para mostrar informes de fallos. Debe configurarse la preferencia <code>breakpad.reportURL</code>.
